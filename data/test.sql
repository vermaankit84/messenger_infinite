-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.16-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5130
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for messenger
CREATE DATABASE IF NOT EXISTS `messenger` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `messenger`;

-- Dumping structure for table messenger.brd_trans
CREATE TABLE IF NOT EXISTS `brd_trans` (
  `MESSAGEID` int(11) NOT NULL,
  `DEPARTMENTCODE` varchar(255) DEFAULT NULL,
  `SENDERNAME` varchar(45) DEFAULT NULL,
  `PHONENUMBER` varchar(45) DEFAULT NULL,
  `MESSAGEPRIORITY` int(10) DEFAULT NULL,
  `MESSAGETEXT` varchar(255) DEFAULT NULL,
  `SIURL` varchar(255) DEFAULT NULL,
  `STATUS` varchar(1) DEFAULT NULL,
  `RETRYATTEMPT` int(11) DEFAULT NULL,
  `INTERNATIONALFLAG` int(11) DEFAULT NULL,
  `MESSAGETYPE` varchar(1) DEFAULT NULL,
  `SUBMITTEDTIME` datetime DEFAULT NULL,
  `CONTENTTYPE` varchar(1) DEFAULT NULL,
  `DNDFLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`),
  KEY `sender_constraint` (`SENDERNAME`),
  KEY `dep_constraint` (`DEPARTMENTCODE`),
  CONSTRAINT `brd_dep_constraint` FOREIGN KEY (`DEPARTMENTCODE`) REFERENCES `mess_division` (`DIVISIONID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `brd_sender_constraint` FOREIGN KEY (`SENDERNAME`) REFERENCES `mess_sender_master` (`SENDER_NAME`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.brd_trans_dump
CREATE TABLE IF NOT EXISTS `brd_trans_dump` (
  `MESSAGEID` int(11) NOT NULL,
  `DEPARTMENTCODE` varchar(255) DEFAULT NULL,
  `SENDERNAME` varchar(45) DEFAULT NULL,
  `PHONENUMBER` varchar(255) DEFAULT NULL,
  `MESSAGEPRIORITY` int(10) DEFAULT NULL,
  `MESSAGETEXT` varchar(255) DEFAULT NULL,
  `SIURL` varchar(255) DEFAULT NULL,
  `STATUS` varchar(1) DEFAULT NULL,
  `RETRYATTEMPT` int(11) DEFAULT NULL,
  `INTERNATIONALFLAG` int(11) DEFAULT NULL,
  `MESSAGETYPE` varchar(1) DEFAULT NULL,
  `SUBMITTEDTIME` datetime DEFAULT NULL,
  `CONTENTTYPE` varchar(1) DEFAULT NULL,
  `DNDFLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_brd_master
CREATE TABLE IF NOT EXISTS `mess_brd_master` (
  `BRDID` int(11) NOT NULL AUTO_INCREMENT,
  `DEPCODE` varchar(45) DEFAULT NULL,
  `SENDERNAME` varchar(45) DEFAULT NULL,
  `STATUS` int(1) DEFAULT NULL,
  `SCHEDULED_DATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `BRD_COUNT` int(11) NOT NULL,
  PRIMARY KEY (`BRDID`),
  KEY `FK_BRD_DIV` (`DEPCODE`),
  KEY `FK_BRD_SEND` (`SENDERNAME`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_brd_temp_table
CREATE TABLE IF NOT EXISTS `mess_brd_temp_table` (
  `msgid` int(11) NOT NULL AUTO_INCREMENT,
  `brdid` int(11) DEFAULT NULL,
  `msgtype` char(1) DEFAULT NULL,
  `destination` varchar(255) DEFAULT NULL,
  `messagetext` varchar(255) DEFAULT NULL,
  `messagepriority` int(1) DEFAULT NULL,
  `intflag` int(1) DEFAULT NULL,
  PRIMARY KEY (`msgid`),
  KEY `FK_brd_temp` (`brdid`),
  CONSTRAINT `FK_brd_temp` FOREIGN KEY (`brdid`) REFERENCES `mess_brd_master` (`BRDID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_bufferconn_master
CREATE TABLE IF NOT EXISTS `mess_bufferconn_master` (
  `BUFFERID` int(11) NOT NULL AUTO_INCREMENT,
  `BUFFERNAME` varchar(45) DEFAULT NULL,
  `BUFFERTABLE` varchar(45) DEFAULT NULL,
  `BUFFERSTATUS` varchar(1) DEFAULT NULL,
  `BUFFERTYPE` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`BUFFERID`),
  UNIQUE KEY `BUFFERNAME_UNIQUE` (`BUFFERNAME`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_buffer_table1
CREATE TABLE IF NOT EXISTS `mess_buffer_table1` (
  `RESPONSEID` varchar(255) NOT NULL,
  `RECEIVEDSDATE` datetime DEFAULT NULL,
  `DELIVERYFLAG` varchar(45) DEFAULT NULL,
  `CONTENTTYPE` varchar(45) DEFAULT NULL,
  `MSISDN` varchar(45) DEFAULT NULL,
  `MSGPRIORITY` varchar(45) DEFAULT NULL,
  `MESSAGETEXT` varchar(255) DEFAULT NULL,
  `DIVISIONID` varchar(255) DEFAULT NULL,
  `INTERNATIONALFLAG` varchar(1) DEFAULT NULL,
  `STATUS` varchar(1) DEFAULT NULL,
  `MESSAGETYPE` varchar(1) DEFAULT NULL,
  `DNDFLAG` varchar(1) DEFAULT NULL,
  `SENDERNAME` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`RESPONSEID`),
  KEY `mess_division_idx` (`DIVISIONID`),
  CONSTRAINT `mess_division` FOREIGN KEY (`DIVISIONID`) REFERENCES `mess_division` (`DIVISIONID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_buffer_table2
CREATE TABLE IF NOT EXISTS `mess_buffer_table2` (
  `RESPONSEID` varchar(255) NOT NULL,
  `RECEIVEDSDATE` datetime DEFAULT NULL,
  `DELIVERYFLAG` varchar(45) DEFAULT NULL,
  `CONTENTTYPE` varchar(45) DEFAULT NULL,
  `MSISDN` varchar(45) DEFAULT NULL,
  `MSGPRIORITY` varchar(45) DEFAULT NULL,
  `MESSAGETEXT` varchar(255) DEFAULT NULL,
  `DIVISIONID` varchar(255) DEFAULT NULL,
  `INTERNATIONALFLAG` varchar(1) DEFAULT NULL,
  `STATUS` varchar(1) DEFAULT NULL,
  `MESSAGETYPE` varchar(1) DEFAULT NULL,
  `DNDFLAG` varchar(1) DEFAULT NULL,
  `SENDERNAME` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`RESPONSEID`),
  KEY `mess_division2_idx` (`DIVISIONID`),
  CONSTRAINT `mess_division2` FOREIGN KEY (`DIVISIONID`) REFERENCES `mess_division` (`DIVISIONID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `mess_buffer_table3` (
  `RESPONSEID` varchar(255) NOT NULL,
  `RECEIVEDSDATE` datetime DEFAULT NULL,
  `DELIVERYFLAG` varchar(45) DEFAULT NULL,
  `CONTENTTYPE` varchar(45) DEFAULT NULL,
  `MSISDN` varchar(45) DEFAULT NULL,
  `MSGPRIORITY` varchar(45) DEFAULT NULL,
  `MESSAGETEXT` varchar(255) DEFAULT NULL,
  `DIVISIONID` varchar(255) DEFAULT NULL,
  `INTERNATIONALFLAG` varchar(1) DEFAULT NULL,
  `STATUS` varchar(1) DEFAULT NULL,
  `MESSAGETYPE` varchar(1) DEFAULT NULL,
  `DNDFLAG` varchar(1) DEFAULT NULL,
  `SENDERNAME` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`RESPONSEID`),
  KEY `mess_division2_idx` (`DIVISIONID`),
  CONSTRAINT `mess_division3` FOREIGN KEY (`DIVISIONID`) REFERENCES `mess_division` (`DIVISIONID`) ON DELETE NO ACTION ON UPDATE NO ACTION
)

CREATE TABLE IF NOT EXISTS `mess_buffer_table4` (
  `RESPONSEID` varchar(255) NOT NULL,
  `RECEIVEDSDATE` datetime DEFAULT NULL,
  `DELIVERYFLAG` varchar(45) DEFAULT NULL,
  `CONTENTTYPE` varchar(45) DEFAULT NULL,
  `MSISDN` varchar(45) DEFAULT NULL,
  `MSGPRIORITY` varchar(45) DEFAULT NULL,
  `MESSAGETEXT` varchar(255) DEFAULT NULL,
  `DIVISIONID` varchar(255) DEFAULT NULL,
  `INTERNATIONALFLAG` varchar(1) DEFAULT NULL,
  `STATUS` varchar(1) DEFAULT NULL,
  `MESSAGETYPE` varchar(1) DEFAULT NULL,
  `DNDFLAG` varchar(1) DEFAULT NULL,
  `SENDERNAME` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`RESPONSEID`),
  KEY `mess_division4_idx` (`DIVISIONID`),
  CONSTRAINT `mess_division4` FOREIGN KEY (`DIVISIONID`) REFERENCES `mess_division` (`DIVISIONID`) ON DELETE NO ACTION ON UPDATE NO ACTION
)
-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_division
CREATE TABLE IF NOT EXISTS `mess_division` (
  `DIVISIONID` varchar(255) NOT NULL,
  `DIVISIONPWD` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`DIVISIONID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_division_vendor_mapper
CREATE TABLE IF NOT EXISTS `mess_division_vendor_mapper` (
  `DIVISIONID` varchar(45) DEFAULT NULL,
  `VENDORID` int(11) DEFAULT NULL,
  KEY `fk_vendor_idx` (`VENDORID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_evtmgr_reqrec
CREATE TABLE IF NOT EXISTS `mess_evtmgr_reqrec` (
  `DIVISIONID` varchar(255) DEFAULT NULL,
  `DATE` varchar(45) DEFAULT NULL,
  `MONTH` varchar(45) DEFAULT NULL,
  `YEAR` varchar(45) DEFAULT NULL,
  `HOUR` varchar(45) DEFAULT NULL,
  `MINUTE` varchar(45) DEFAULT NULL,
  `MESSAGES` int(11) DEFAULT NULL,
  KEY `mess_div_constraint_idx` (`DIVISIONID`),
  CONSTRAINT `mess_div_constraint` FOREIGN KEY (`DIVISIONID`) REFERENCES `mess_division` (`DIVISIONID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_evtmgr_vendordowntime
CREATE TABLE IF NOT EXISTS `mess_evtmgr_vendordowntime` (
  `VendorId` int(11) DEFAULT NULL,
  `RecordId` int(11) NOT NULL AUTO_INCREMENT,
  `DowntimeRequest` datetime DEFAULT NULL,
  `DowntimeDate` varchar(50) DEFAULT NULL,
  `DowntimeMonth` varchar(50) DEFAULT NULL,
  `DowntimeYear` varchar(50) DEFAULT NULL,
  `DowntimeHour` varchar(50) DEFAULT NULL,
  `DowntimeMinute` varchar(50) DEFAULT NULL,
  `TotalDowntime` varchar(50) DEFAULT NULL,
  `Uptimerequest` datetime DEFAULT NULL,
  `Uptimemonth` varchar(50) DEFAULT NULL,
  `Uptimeyear` varchar(50) DEFAULT NULL,
  `UpTimeHour` varchar(50) DEFAULT NULL,
  `Upttimeminute` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`RecordId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_evtmgr_vendorfailure
CREATE TABLE IF NOT EXISTS `mess_evtmgr_vendorfailure` (
  `vendorId` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `month` varchar(50) NOT NULL,
  `year` varchar(50) NOT NULL,
  `hour` varchar(50) NOT NULL,
  `minute` varchar(50) NOT NULL,
  `totalcount` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_evtmgr_vendorsuccess
CREATE TABLE IF NOT EXISTS `mess_evtmgr_vendorsuccess` (
  `vendorid` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `month` varchar(50) NOT NULL,
  `year` varchar(50) NOT NULL,
  `hour` varchar(50) NOT NULL,
  `minute` varchar(50) NOT NULL,
  `primarycount` int(11) NOT NULL,
  `secondarycount` int(11) NOT NULL,
  `totalcount` int(11) NOT NULL,
  KEY `FKvendor` (`vendorid`),
  CONSTRAINT `FKvendor` FOREIGN KEY (`vendorid`) REFERENCES `mess_vendor_master` (`VENDORID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_sender_master
CREATE TABLE IF NOT EXISTS `mess_sender_master` (
  `SENDER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SENDER_NAME` varchar(45) DEFAULT NULL,
  `SENDER_IS_ACTIVE` int(1) DEFAULT NULL,
  `DIVISIONID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SENDER_ID`),
  UNIQUE KEY `SENDER_NAME_UNIQUE` (`SENDER_NAME`),
  KEY `mess_sender_div_idx` (`DIVISIONID`),
  CONSTRAINT `mess_send_div` FOREIGN KEY (`DIVISIONID`) REFERENCES `mess_division` (`DIVISIONID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_template_master
CREATE TABLE IF NOT EXISTS `mess_template_master` (
  `TEMP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TEMPLATE_NAME` varchar(45) DEFAULT NULL,
  `TEMPLATE_DETAILS` varchar(45) DEFAULT NULL,
  `TEMPLATE_IS_ACTIVE` int(1) DEFAULT NULL,
  `DIVISIONID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TEMP_ID`),
  UNIQUE KEY `TEMPLATE_NAME_UNIQUE` (`TEMPLATE_NAME`),
  KEY `mess_temp_div_idx` (`DIVISIONID`),
  CONSTRAINT `mess_temp_div` FOREIGN KEY (`DIVISIONID`) REFERENCES `mess_division` (`DIVISIONID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mess_vendor_master
CREATE TABLE IF NOT EXISTS `mess_vendor_master` (
  `VENDORID` int(11) NOT NULL AUTO_INCREMENT,
  `VENDORNAME` varchar(45) DEFAULT NULL,
  `VENDORORIGIN` varchar(45) DEFAULT NULL,
  `VENDORTYPE` varchar(1) DEFAULT NULL,
  `VENDORPRIMARYURL` varchar(255) DEFAULT NULL,
  `VENDORSECONDARYURL` varchar(255) DEFAULT NULL,
  `VENDORPRIORITY` varchar(45) DEFAULT NULL,
  `VENDORLOAD` varchar(45) DEFAULT NULL,
  `VENDORSTATUS` varchar(1) DEFAULT NULL,
  `COUNTRYROUTINGFLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`VENDORID`),
  UNIQUE KEY `VENDORNAME_UNIQUE` (`VENDORNAME`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mis_rec_log
CREATE TABLE IF NOT EXISTS `mis_rec_log` (
  `MESSAGEID` varchar(255) NOT NULL,
  `APPRESPONSEID` varchar(255) DEFAULT NULL,
  `DIVISIONID` varchar(255) DEFAULT NULL,
  `DELIVERYFLAG` varchar(45) DEFAULT NULL,
  `MSGTYPE` varchar(1) DEFAULT NULL,
  `MSISDN` varchar(45) DEFAULT NULL,
  `PRIORITY` varchar(45) DEFAULT NULL,
  `MESSAGETEXT` varchar(255) DEFAULT NULL,
  `SMSLENGTH` int(11) DEFAULT NULL,
  `DNDFLAG` varchar(255) DEFAULT NULL,
  `INTFLAG` varchar(1) DEFAULT NULL,
  `REQDUMPDATETIME` datetime DEFAULT NULL,
  `SENDERNAME` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.mis_sub_log
CREATE TABLE IF NOT EXISTS `mis_sub_log` (
  `MESSAGEID` varchar(255) NOT NULL,
  `MSISDN` varchar(45) DEFAULT NULL,
  `STATUS` varchar(45) DEFAULT NULL,
  `STATUSDESC` varchar(45) DEFAULT NULL,
  `VENDORID` varchar(45) DEFAULT NULL,
  `VENDORRESPONSEID` varchar(45) DEFAULT NULL,
  `VENDORSUBMITDATETIME` varchar(45) DEFAULT NULL,
  `REQDUMPDATETIME` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.real_time_transaction
CREATE TABLE IF NOT EXISTS `real_time_transaction` (
  `MESSAGEID` int(11) NOT NULL AUTO_INCREMENT,
  `DEPARTMENTCODE` varchar(255) DEFAULT NULL,
  `SENDERNAME` varchar(45) DEFAULT NULL,
  `PHONENUMBER` varchar(15) DEFAULT NULL,
  `MESSAGEPRIORITY` int(10) DEFAULT NULL,
  `MESSAGETEXT` varchar(255) DEFAULT NULL,
  `SIURL` varchar(255) DEFAULT NULL,
  `STATUS` varchar(1) DEFAULT NULL,
  `RETRYATTEMPT` int(11) DEFAULT NULL,
  `INTERNATIONALFLAG` int(11) DEFAULT NULL,
  `MESSAGETYPE` varchar(1) DEFAULT NULL,
  `SUBMITTEDTIME` datetime DEFAULT NULL,
  `CONTENTTYPE` varchar(1) DEFAULT NULL,
  `DNDFLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`),
  KEY `sender_constraint` (`SENDERNAME`),
  KEY `dep_constraint` (`DEPARTMENTCODE`),
  CONSTRAINT `dep_constraint` FOREIGN KEY (`DEPARTMENTCODE`) REFERENCES `mess_division` (`DIVISIONID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sender_constraint` FOREIGN KEY (`SENDERNAME`) REFERENCES `mess_sender_master` (`SENDER_NAME`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table messenger.real_time_transaction_dump
CREATE TABLE IF NOT EXISTS `real_time_transaction_dump` (
  `MESSAGEID` int(11) NOT NULL DEFAULT '0',
  `DEPARTMENTCODE` varchar(255) DEFAULT NULL,
  `SENDERNAME` varchar(45) DEFAULT NULL,
  `PHONENUMBER` varchar(15) DEFAULT NULL,
  `MESSAGEPRIORITY` int(10) DEFAULT NULL,
  `MESSAGETEXT` varchar(255) DEFAULT NULL,
  `SIURL` varchar(255) DEFAULT NULL,
  `STATUS` varchar(1) DEFAULT NULL,
  `RETRYATTEMPT` int(11) DEFAULT NULL,
  `INTERNATIONALFLAG` int(11) DEFAULT NULL,
  `MESSAGETYPE` varchar(1) DEFAULT NULL,
  `SUBMITTEDTIME` datetime DEFAULT NULL,
  `CONTENTTYPE` varchar(1) DEFAULT NULL,
  `DNDFLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
