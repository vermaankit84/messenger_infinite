package com.messenger.response;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.messenger.constants.VendorOrigin;
import com.messenger.repositories.VendorRepository;
import com.messenger.response.vendor.ez.ResponseEz;
import com.messenger.valueobjects.SendGridBean;

public final class ParseVendorResponse {
	
	private final Logger logger = Logger.getLogger(ParseVendorResponse.class.getName());
	public final String ParseResponseId(final String urlresponse, final int vendorId) {
		final VendorOrigin vendorOrigin = VendorOrigin.valueOf(VendorRepository.getInstance().getVendorOrign(vendorId).toUpperCase());
		String response = null;
		try {
			switch (vendorOrigin) {
			case ACL:
				response = parseAclResponse(urlresponse);
				break;
			case SENDGRID:
				response = parseSendergridresponse(urlresponse);
				break;
			case TWILITO:
				response = parseTwilitoResponse(urlresponse);
				break;
			case ZANG:
				response = parseZangResponse(urlresponse);
				break;
			case EZ:
				response = parseEzResponse(urlresponse);
				break;
			default:
				throw new UnsupportedOperationException("This [ " + vendorOrigin + " ]  is not supported ");
			}
		} catch (Exception e) {
			logger.warn("Exception arises while getting response id" , e);
		}

		return response;
	}

	private String parseEzResponse(final String urlresponse) {
		try {
			final Unmarshaller unmarshaller = JAXBContext.newInstance(ResponseEz.class).createUnmarshaller();
			ResponseEz r = (ResponseEz) unmarshaller.unmarshal(new StringReader(urlresponse));
			if (r!=null && "success".equalsIgnoreCase(r.getStatus())) {
				return (r.getEntry().getID());
			}			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String parseZangResponse(final String urlresponse) {
		try {
			return ((com.messenger.response.vendor.zang.Response) JAXBContext.newInstance(com.messenger.response.vendor.zang.Response.class).
					createUnmarshaller().unmarshal(new StringReader(urlresponse))).getSmsMessage().getAccountSid();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return null;
        
	}

	private String parseSendergridresponse(final String urlresponse) {
		final Gson gson = new Gson();
		final SendGridBean sendGridBean = gson.fromJson(urlresponse, SendGridBean.class);
		if (sendGridBean!=null) {
			return sendGridBean.getMessage();
		}
		return null;
	}

	private  String parseTwilitoResponse(String response) {
		String strResponse = null;
		if (response!=null && !response.isEmpty()) {
			String [] strData = response.split("\\,");
	        for (String s: strData) {
	            if (s.contains("sid")) {
	                String sid  = s.split(":")[1];
	                strResponse = sid.substring(2 , sid.length() - 1);
	                break;
	            }
	        }
		}		
        return strResponse;
	}	
	
	
	private  String parseAclResponse(String response) {		
		String strResponse = null;
		if (response!=null && !response.isEmpty()) {
			strResponse = response.split(":")[1];
		}
		return strResponse;
	}
}
