package com.messenger.response.vendor.ez;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by ankverma on 11/18/2016.
 */
@XmlRootElement(name = "Response")
public class ResponseEz {

    @XmlElement
    private String Status;
    @XmlElement
    private Entry Entry;
    @XmlElement
    private String Code;

    public String getStatus ()
    {
        return Status;
    }

    public void setStatus (String Status)
    {
        this.Status = Status;
    }

    public Entry getEntry ()
    {
        return Entry;
    }

    public void setEntry (Entry Entry)
    {
        this.Entry = Entry;
    }

    public String getCode ()
    {
        return Code;
    }

    public void setCode (String Code)
    {
        this.Code = Code;
    }

    @Override
    public String toString()
    {
        return "ResponseEz [Status = "+Status+", Entry = "+Entry+", Code = "+Code+"]";
    }
}
