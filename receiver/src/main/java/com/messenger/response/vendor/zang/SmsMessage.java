package com.messenger.response.vendor.zang;

import javax.xml.bind.annotation.XmlElement;


public class SmsMessage
{
    @XmlElement
    private String AccountSid;
    @XmlElement
    private String Status;
    @XmlElement
    private String Body;
    @XmlElement
    private String DateSent;
    @XmlElement
    private String Price;
    @XmlElement
    private String Uri;
    @XmlElement
    private String To;
    @XmlElement
    private String Sid;
    @XmlElement
    private String DateCreated;
    @XmlElement
    private String DateUpdated;
    @XmlElement
    private String Direction;
    @XmlElement
    private String From;
    @XmlElement
    private String ApiVersion;

    public String getAccountSid ()
    {
        return AccountSid;
    }

    public void setAccountSid (String AccountSid)
    {
        this.AccountSid = AccountSid;
    }

    public String getStatus ()
    {
        return Status;
    }

    public void setStatus (String Status)
    {
        this.Status = Status;
    }

    public String getBody ()
    {
        return Body;
    }

    public void setBody (String Body)
    {
        this.Body = Body;
    }

    public String getDateSent ()
    {
        return DateSent;
    }

    public void setDateSent (String DateSent)
    {
        this.DateSent = DateSent;
    }

    public String getPrice ()
    {
        return Price;
    }

    public void setPrice (String Price)
    {
        this.Price = Price;
    }

    public String getUri ()
    {
        return Uri;
    }

    public void setUri (String Uri)
    {
        this.Uri = Uri;
    }

    public String getTo ()
    {
        return To;
    }

    public void setTo (String To)
    {
        this.To = To;
    }

    public String getSid ()
    {
        return Sid;
    }

    public void setSid (String Sid)
    {
        this.Sid = Sid;
    }

    public String getDateCreated ()
    {
        return DateCreated;
    }

    public void setDateCreated (String DateCreated)
    {
        this.DateCreated = DateCreated;
    }

    public String getDateUpdated ()
    {
        return DateUpdated;
    }

    public void setDateUpdated (String DateUpdated)
    {
        this.DateUpdated = DateUpdated;
    }

    public String getDirection ()
    {
        return Direction;
    }

    public void setDirection (String Direction)
    {
        this.Direction = Direction;
    }

    public String getFrom ()
    {
        return From;
    }

    public void setFrom (String From)
    {
        this.From = From;
    }

    public String getApiVersion ()
    {
        return ApiVersion;
    }

    public void setApiVersion (String ApiVersion)
    {
        this.ApiVersion = ApiVersion;
    }

    @Override
    public String toString()
    {
        return "SmsMessage [AccountSid = "+AccountSid+", Status = "+Status+", Body = "+Body+", DateSent = "+DateSent+", Price = "+Price+", Uri = "+Uri+", To = "+To+", Sid = "+Sid+", DateCreated = "+DateCreated+", DateUpdated = "+DateUpdated+", Direction = "+Direction+", From = "+From+", ApiVersion = "+ApiVersion+"]";
    }
}
