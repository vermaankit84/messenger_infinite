package com.messenger.response.vendor.ez;

/**
 * Created by ankverma on 11/18/2016.
 */
public class Entry {
    private String Groups;

    private String RecipientsCount;

    private String Credits;

    private String Subject;

    private String Message;

    private String ID;

    private String StampToSend;

    private String MessageTypeID;

    private PhoneNumbers PhoneNumbers;

    public String getGroups ()
    {
        return Groups;
    }

    public void setGroups (String Groups)
    {
        this.Groups = Groups;
    }

    public String getRecipientsCount ()
    {
        return RecipientsCount;
    }

    public void setRecipientsCount (String RecipientsCount)
    {
        this.RecipientsCount = RecipientsCount;
    }

    public String getCredits ()
    {
        return Credits;
    }

    public void setCredits (String Credits)
    {
        this.Credits = Credits;
    }

    public String getSubject ()
    {
        return Subject;
    }

    public void setSubject (String Subject)
    {
        this.Subject = Subject;
    }

    public String getMessage ()
    {
        return Message;
    }

    public void setMessage (String Message)
    {
        this.Message = Message;
    }

    public String getID ()
    {
        return ID;
    }

    public void setID (String ID)
    {
        this.ID = ID;
    }

    public String getStampToSend ()
    {
        return StampToSend;
    }

    public void setStampToSend (String StampToSend)
    {
        this.StampToSend = StampToSend;
    }

    public String getMessageTypeID ()
    {
        return MessageTypeID;
    }

    public void setMessageTypeID (String MessageTypeID)
    {
        this.MessageTypeID = MessageTypeID;
    }

    public PhoneNumbers getPhoneNumbers ()
    {
        return PhoneNumbers;
    }

    public void setPhoneNumbers (PhoneNumbers PhoneNumbers)
    {
        this.PhoneNumbers = PhoneNumbers;
    }

    @Override
    public String toString()
    {
        return "Entry [Groups = "+Groups+", RecipientsCount = "+RecipientsCount+", Credits = "+Credits+", Subject = "+Subject+", Message = "+Message+", ID = "+ID+", StampToSend = "+StampToSend+", MessageTypeID = "+MessageTypeID+", PhoneNumbers = "+PhoneNumbers+"]";
    }
}
