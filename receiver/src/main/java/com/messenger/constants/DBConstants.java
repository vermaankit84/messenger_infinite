package com.messenger.constants;

public enum DBConstants {
	LOOK_VALUE("java:/comp/env"), DATA_VALUE("aggregator") ;

	private int key = 0;
	private String value = null;

	private DBConstants() {
	}

	private DBConstants(int key) {
		this.key = key;
	}

	private DBConstants(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	public int key() {
		return this.key;
	}

}
