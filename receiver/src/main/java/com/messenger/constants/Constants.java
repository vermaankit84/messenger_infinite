package com.messenger.constants;

public final class Constants {

	private Constants() {}
	
	public static boolean IS_RT_ENABLED = true;
	public static boolean IS_BRD_ENABLED = true;
	
	public static String STR_MODE = System.getenv("mode");
}
