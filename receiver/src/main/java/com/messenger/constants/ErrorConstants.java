package com.messenger.constants;


public interface ErrorConstants {

	String MESSENGER_ERROR_MISSING_DIVISIONID = "Invalid or Missing Division ID";
	String MESSENGER_ERROR_MISSING_SUB_DIVISIONID = "Invalid or Missing Sub Division ID";
	String MESSENGER_ERROR_MISSING_PASSWORD = "Invalid or Missing Password";
	String MESSENGER_ERROR_MISSING_SENDER = "Invalid or Missing Sender";
	String MESSENGER_ERROR_MISSING_MOBILENUMBER = "Invalid or Missing Mobile Number";
	String MESSENGER_ERROR_MISSING_MESSAGETEXT = "Invalid or Missing Message Text";
	String MESSENGER_ERROR_MAILFORMED_XML_DATA_RECEIVED = "Invalid or malformed Xml Obtained";
	String MESSENGER_ERROR_DIVSION_ID_MISMATCH = "Invalid Division ID obtained";
	String MESSENGER_EMAIL_TEMPLATE_ID_MISSING = "Invalid or Missing Template Id obtained";
	String MESSENGER_INVALID_MSG_TYPE = "Message type can only be S -> SMS or E -> Email";
	String MESSENGER_ERROR_INTERNATIONAL_FLAG_NOT_FOUND = "If message type -> S Intflag is mandatory";
	String MESSENGER_ERROR_AUTHENTICATION_FAILED = "Mismatch Division Id and Subdivision ID obtained";
	String MESSENGER_ERROR_INTERNATIONAL_FLAG_NOT_VALID = "International Flag can only be 1 --> National and 2 --> International";
	String MESSENGER_DND_FLAG_NOT_WITH_EMAIL = "DND flag cannot be applied with Email";
	String MESSENGER_DND_FLAG_INVALID = "Dnd Flag can only be 1 or 2 or 3 1 [OTP] 2[Transactional] 3[Promo]";
	String MESSENGER_INVALID_MSIDN_RECEIVED = "Invalid Msisdn Received";
}
