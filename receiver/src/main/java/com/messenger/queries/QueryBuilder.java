package com.messenger.queries;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.apache.log4j.Logger;

import com.messenger.db.DBConn;
import com.messenger.utils.ReceiverUtils;

public final class QueryBuilder {

    private QueryBuilder() {
    }
    private final static Logger logger = Logger.getLogger(QueryBuilder.class.getName());

    public static void insertIntoMisSubLog(String mMessageId, String pMobile, String status, String statusDesc , String vendorResponse , String vendorid , String vendorsubmittime) {
        String strQuery = "INSERT INTO mis_sub_log (MESSAGEID , MSISDN , STATUS , STATUSDESC  ,VENDORID,VENDORRESPONSEID,VENDORSUBMITDATETIME, REQDUMPDATETIME)VALUES (? ,? ,? ,? ,?,?,DATE_FORMAT (now(), '%Y-%c-%d %H:%i:%s'), DATE_FORMAT (?, '%Y-%c-%d %H:%i:%s'))";
        Connection conn = null;
        PreparedStatement prep = null;
        int noOfRows = 0;
		try {
			conn = DBConn.getconnetion();
			prep = conn.prepareStatement(strQuery);
			prep.setString(1, mMessageId);
			prep.setString(2, pMobile);
			prep.setString(3, status);
			prep.setString(4, statusDesc);
			prep.setString(5, vendorid);
			prep.setString(6, vendorResponse);
			prep.setString(7, vendorsubmittime);
			noOfRows = prep.executeUpdate();
		} catch (Exception ex) {
			logger.warn("Exception Arises while saving data in MIS table", ex);
        } finally {
            DBConn.releaseDbConnection(null, null, conn, prep);
        }

        if (noOfRows > 0) {
        	logger.info("Messenger no of records inserted for messageId [ " + mMessageId + " ] Mobile Number [ " + pMobile + " ] is successfully for sub log");
        }
    }

    public static void insertInMisRecLog(String pMessageId, String pResponseId, String pDivisionId, String pReceivedSender, String pReqDumpTime, String pDeliveryFlag, String pMessageType, String pMSISDN, String pPriority, String pMessageText, String pSMSCount, String pDNDFlag, String intFlag , String vendorOrigin) {

        Connection conn = null;
        PreparedStatement prep = null;
        int noOfRows = 0;
        String strQuery =  "INSERT INTO MIS_REC_LOG (MESSAGEID , APPRESPONSEID , DIVISIONID , DELIVERYFLAG , MSGTYPE, MSISDN, PRIORITY, MESSAGETEXT, SMSLENGTH, DNDFLAG, INTFLAG, REQDUMPDATETIME , SENDERNAME , VENDORORIGIN) VALUES (? , ? , ?, ? , ? , ? , ? , ? , ?, ? , ? ,   DATE_FORMAT(?, '%Y-%c-%d %H:%i:%s') , ? , ?)";
      
		try {
			conn = DBConn.getconnetion();
			prep = conn.prepareStatement(strQuery);
			prep.setString(1, pMessageId);
			prep.setString(2, pResponseId);
			prep.setString(3, pDivisionId);
			prep.setString(4, pDeliveryFlag);
			prep.setString(5, pMessageType);
			prep.setString(6, pMSISDN);
			prep.setString(7, pPriority);
			prep.setString(8, pMessageText);
			prep.setString(9, pSMSCount);
			prep.setString(10, pDNDFlag);
			prep.setString(11, intFlag);
			prep.setString(12, ReceiverUtils.getCurrentDateTime());
			prep.setString(13, pReceivedSender);
			prep.setString(14, vendorOrigin);
			noOfRows = prep.executeUpdate();

		} catch (Exception ex) {
			logger.warn("*************** MESSENGER EXCEPTION ARISES WHILE INSERTING DATA IN MIS REC LOG *************** for message id " + pMessageId , ex);
        } finally {
            DBConn.releaseDbConnection(null, null, conn, prep);
        }
        if (noOfRows > 0) {
        	logger.info("Messenger no of records inserted for messageId [ " + pMessageId + " ] Mobile Number [ " + pMSISDN + " ] is successfully for rec log");
        }
    }

    public static void insertInBufferLog(String pMessageId, String pReceivedSubmitDate, String pDLR, String pContentId, String pMobile, String pPriority, String pMessageText, String pDivisionId, String pInternationalFlag, String pMessageType, String pBufferTableName, String mSenderId, String mDndFlag) {       
    	Connection conn = null;
        PreparedStatement prep = null;
        int noOfRows = 0;

        try {
            conn = DBConn.getconnetion();
            prep = conn.prepareStatement("INSERT INTO %pBufferTableName% (RESPONSEID,RECEIVEDSDATE,DELIVERYFLAG,CONTENTTYPE,MSISDN ,MSGPRIORITY,MESSAGETEXT,DIVISIONID,INTERNATIONALFLAG,STATUS,MESSAGETYPE,DNDFLAG,SENDERNAME) values( ? , DATE_FORMAT(?, '%Y-%c-%d %H:%i:%s') , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?,?)".replaceAll("%pBufferTableName%", pBufferTableName));
            prep.setString(1, pMessageId);
            prep.setString(2, ReceiverUtils.getCurrentDateTime());
			prep.setString(3, pDLR);
			prep.setString(4, pContentId);
			prep.setString(5, pMobile);
			prep.setString(6, pPriority);
			prep.setString(7, pMessageText);
			prep.setString(8, pDivisionId);
			prep.setString(9, pInternationalFlag);
			prep.setString(10, "N");
			prep.setString(11, pMessageType);
			prep.setString(12, mDndFlag);
			prep.setString(13, mSenderId);
            noOfRows = prep.executeUpdate();

        } catch (Exception ex) {
        	logger.warn("******* MESSENGER EXCEPTION ARISES IN BUFFER MANAGER QUERY *********************" , ex);
        } finally {
            DBConn.releaseDbConnection(null, null, conn, prep);
        }

        if (noOfRows > 0) {
        	logger.info("Messenger no of records inserted for messageId [ " + pMessageId + " ] Mobile Number [ " + pMobile + " ] is successfully for queue [ " + pBufferTableName + " ] ");
        }
    }
}
