package com.messenger.httphandler;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import com.messenger.utils.ReceiverUtils;



public final class HttpClientManager {

	private static HttpClientManager httpClientManagerInstance = null;
	private  SSLContext sslContext = null;
	private PoolingHttpClientConnectionManager poolClientConnectionManager = null;
	private final Logger logger = Logger.getLogger(HttpClientManager.class.getName());
	private final ResponseHandler<String> respHandler= new BasicResponseHandler();
	private RequestConfig config = null;
	private CloseableHttpClient closeableHttpClient = null;
	private TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public void checkClientTrusted(
				java.security.cert.X509Certificate[] certs, String authType) {
		}

		public void checkServerTrusted(
				java.security.cert.X509Certificate[] certs, String authType) {
		}
	} };
		
	public static synchronized HttpClientManager getInstance() {
		if (httpClientManagerInstance == null) {
			httpClientManagerInstance = new HttpClientManager();
		}
		return httpClientManagerInstance;
	}
	
	private HttpClientManager() {     
	        
	}

	public final void buildClientManager() {
		try {
		    sslContext = SSLContext.getInstance("SSL");
		    sslContext.init(null, trustAllCerts, new SecureRandom());
		} catch (KeyManagementException ex) {
		    ex.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		final SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		final PlainConnectionSocketFactory pcsf = PlainConnectionSocketFactory.getSocketFactory();
		final Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create().register("http", pcsf).register("https", sslsf).build();
		poolClientConnectionManager = new PoolingHttpClientConnectionManager(registry);
		poolClientConnectionManager.setMaxTotal(200);
		poolClientConnectionManager.setDefaultMaxPerRoute(30);
		poolClientConnectionManager.closeExpiredConnections();
		poolClientConnectionManager.closeIdleConnections(10, TimeUnit.SECONDS);
		config =  RequestConfig.custom().setConnectTimeout(5000).setSocketTimeout(5000).build();		
	}
	
	
	public String generatePostResponse(final String pushurl , final Map<String, String> headerMap , final Map<String, String> namevaluePairs , final String crediantails) throws IOException {	
		final HttpPost httpPost = new HttpPost(pushurl);
		
		if (crediantails!=null && !crediantails.isEmpty()) {
			final  CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
			credentialsProvider.setCredentials(AuthScope.ANY,  new UsernamePasswordCredentials(crediantails.split(",")[0], crediantails.split(",")[1]));
			closeableHttpClient = HttpClientBuilder.create().setConnectionManager(poolClientConnectionManager).setDefaultRequestConfig(config).setDefaultCredentialsProvider(credentialsProvider).build();
		} else {
			closeableHttpClient = HttpClientBuilder.create().setConnectionManager(poolClientConnectionManager).setDefaultRequestConfig(config).build();
		}
		if (headerMap!=null && !headerMap.isEmpty()) {
			final Iterator<String> itr = headerMap.keySet().iterator();
			while(itr.hasNext()) {
				String key = itr.next();
				httpPost.addHeader(key, headerMap.get(key));
			}
		}
		final List<NameValuePair> urlParameters = new ArrayList<>();
		if (namevaluePairs!=null && !namevaluePairs.isEmpty()) {
			final Iterator<String> itr = namevaluePairs.keySet().iterator();
			while(itr.hasNext()) {
				String key = itr.next();
				urlParameters.add(new BasicNameValuePair(key, namevaluePairs.get(key)));
			}
		}		
		String strResponse = null;
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
			strResponse = closeableHttpClient.execute(httpPost,respHandler);
		} catch (Exception e) {
			logger.warn("Exception in vendor call on "+ReceiverUtils.getCurrentDateTime()+" caused by " , e);
		} finally {
			if(httpPost!=null){
				httpPost.releaseConnection();
			}	
			
		}
		return strResponse;
		
	}

	public String generateGetResponse(String pushurl) throws IOException {
		final HttpGet httpGet = new HttpGet(pushurl);
		String strResponse = null;
		closeableHttpClient = HttpClientBuilder.create().setConnectionManager(poolClientConnectionManager).setDefaultRequestConfig(config).build();
		try {
			strResponse = closeableHttpClient.execute(httpGet,respHandler);
		} catch (Exception e) {
			logger.warn("Exception in vendor call on "+ReceiverUtils.getCurrentDateTime()+" caused by " , e);
		} finally {
			if(httpGet!=null){
				httpGet.releaseConnection();
			}				
		}
		return strResponse;
	}

	public void closepoolconnection() throws IOException {
		if (closeableHttpClient != null) {
			closeableHttpClient.close();
		}
		if (poolClientConnectionManager != null) {
			poolClientConnectionManager.close();
		}
	}
}
