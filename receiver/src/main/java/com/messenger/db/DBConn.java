package com.messenger.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;


public final class DBConn {
	private static DataSource dataSource = null;
	private final static Logger logger = Logger.getLogger(DBConn.class.getName());
	
	public static Connection getconnetion() throws Exception {
		Connection conn = null;
		try {
			if (dataSource != null) {
				conn = dataSource.getConnection();
			}
			if (conn == null){
				throw new Exception("Messanger DbConnection::::getconnetion NOT ABLE TO GET THE DB CONNECTION.");
			}
			
		} catch (Exception e) {
			logger.warn("Exception arises while getting db Connection::::getconnetion" + e);
			throw new Exception(e);
		}
		return conn;
	}

	public static void setDataSource(DataSource dataSource) {
		DBConn.dataSource = dataSource;
	}

	public static void releaseDbConnection(Statement st, ResultSet rs,Connection conn, PreparedStatement psmt) {

		try {
			if (st != null) {
				st.close();
				st = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			st = null;
		}

		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			rs = null;
		}

		try {
			if (psmt != null) {
				psmt.close();
				psmt = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			psmt = null;
		}

		try {
			if (conn != null) {
				conn.close();
				conn = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			conn = null;
		}
		
	}

	public static void closeDataSource() {
		if (DBConn.dataSource != null)
			DBConn.dataSource.close();
	}
}