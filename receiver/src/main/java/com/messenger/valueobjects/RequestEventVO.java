package com.messenger.valueobjects;

public final class RequestEventVO {

	private int mCounter;

	public RequestEventVO(int pRequestCount) {
		this.mCounter = pRequestCount;
	}

	public int getRequestCount() {
		return mCounter;
	}

	public void setRequestCount(int pRequestCount) {
		this.mCounter = pRequestCount;
	}

	@Override
	public String toString() {
		return "RequestEventVO [mCounter=" + mCounter + "]";
	}

	
}
