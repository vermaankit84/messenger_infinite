package com.messenger.valueobjects;

public final class DivisionVO {

	private String pdivisionId = null;
	private String ppassword = null;
	
	public DivisionVO(final String pdivisionId, final String ppassword) {
		this.pdivisionId = pdivisionId;
		this.ppassword = ppassword;
	}

	public String getPdivisionId() {
		return pdivisionId;
	}


	public String getPpassword() {
		return ppassword;
	}

	@Override
	public String toString() {
		return "DivisionVO [pdivisionId=" + pdivisionId + ", ppassword=" + ppassword + "]";
	}

	

}
