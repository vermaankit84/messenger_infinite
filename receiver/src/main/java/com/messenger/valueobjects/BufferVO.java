package com.messenger.valueobjects;

public class BufferVO {
	private String bufferName;
	private String bufferTable;
	private String bufferStatus;
	private String bufferType;
 

	public BufferVO(String bufferName, String bufferTable, String bufferStatus,String bufferType) {
		this.bufferName = bufferName;
		this.bufferTable = bufferTable;
		this.bufferStatus = bufferStatus;
		this.bufferType = bufferType;
	 }

	public String getBufferName() {
		return bufferName;
	}

	public String getBufferTable() {
		return bufferTable;
	}

	public String getBufferStatus() {
		return bufferStatus;
	}

	public String getBufferType() {
		return bufferType;
	}

	@Override
	public String toString() {
		return "BufferVO [bufferName=" + bufferName + ", bufferTable=" + bufferTable + ", bufferStatus=" + bufferStatus
				+ ", bufferType=" + bufferType + "]";
	}

	 
	
}
