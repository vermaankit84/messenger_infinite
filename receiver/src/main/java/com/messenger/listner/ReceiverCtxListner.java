package com.messenger.listner;

import java.io.IOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;

import com.messenger.constants.DBConstants;
import com.messenger.db.DBConn;
import com.messenger.servers.ReceiverRequest;
import com.messenger.utils.ReceiverUtils;

public final class ReceiverCtxListner implements ServletContextListener {

	private final Logger logger = Logger.getLogger(ReceiverCtxListner.class.getName());
	@Override
	public final void contextDestroyed(ServletContextEvent sce) {
		try {
			ReceiverRequest.getReceiverRequest().stopServer();
		} catch (IOException e) {
			logger.warn("Exception arises while stoppingserver " , e);
		}
		final ServletContext ctx = sce.getServletContext(); 
    	ctx.removeAttribute(DBConstants.DATA_VALUE.value()); 
    	DBConn.closeDataSource();
	}

	@Override
	public final void contextInitialized(ServletContextEvent arg0) {
		try {
			final Context iniCon = new InitialContext();
			final Context ctx = (Context) iniCon.lookup(DBConstants.LOOK_VALUE.value());
			if (ctx != null) {
				final DataSource dataSource = (DataSource) ctx.lookup(DBConstants.DATA_VALUE.value());
				logger.info("DataSource Obtained [ " + dataSource + " ] at [ " + ReceiverUtils.getCurrentDateTime() + " ] ");
				DBConn.setDataSource(dataSource);
			}
			ReceiverRequest.getReceiverRequest().startServer();
		} catch (NamingException e) {
			logger.warn("Exception arises while loading database context " , e);
		}
	}
}
