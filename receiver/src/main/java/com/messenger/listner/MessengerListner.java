package com.messenger.listner;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.messenger.defaulthandler.Abstracthandler;
import com.messenger.pool.ListnerThreadPool;
import com.messenger.utils.ReceiverUtils;

public class MessengerListner extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String CONTENT_TYPE = "text/plain";
	private final Logger logger = Logger.getLogger(MessengerListner.class.getName());
	@Override
	public void init() throws ServletException {
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType(CONTENT_TYPE);
		final PrintWriter out = response.getWriter();
		Map<String, String> reqRec = ReceiverUtils.getRequestParameters(request);
		String strResponse = null;	 
		if (reqRec != null && reqRec.size() > 0) {
			logger.info("Messenger Request Obtained [ " + reqRec + " ] at [ " + ReceiverUtils.getCurrentDateTime() + " ] ");
			final Abstracthandler abstracthandler = ReceiverUtils.getHandler(request, reqRec);
			Future<String> futureResponse = null;
			try {
				futureResponse = abstracthandler != null ?  ListnerThreadPool.getInstance().getPool().submit(abstracthandler) : null;
				if (futureResponse != null) {
					strResponse = futureResponse.get();
				}
			} catch (RejectedExecutionException | InterruptedException | ExecutionException e) {
				logger.warn("Exception arises while submitting request [ " + reqRec + " ] ", e);
				strResponse = "Thread pool is full please fire request after some time";
			} 
		}	
		logger.info("Messenger Response Obtained for request [ " + reqRec + "] [ " + (strResponse!=null && !strResponse.isEmpty() ? strResponse : "No response obtained")  + " ] ");
		out.write(strResponse!=null && !strResponse.isEmpty() ? strResponse : "no reponse obtained");
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	public void destroy() {
	}

}
