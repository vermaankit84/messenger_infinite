package com.messenger.listner;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.messenger.constants.Constants;
import com.messenger.repositories.BufferRepository;
import com.messenger.repositories.DivisionRepository;
import com.messenger.repositories.SenderRepository;
import com.messenger.repositories.TemplateRepository;
import com.messenger.repositories.VendorRepository;
import com.messenger.utils.ReceiverUtils;

public final class RefreshListner extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String CONTENT_TYPE = "text/plain";
	private final Logger logger = Logger.getLogger(RefreshListner.class.getName());
	
	@Override
	public void init() throws ServletException {
	}
	
	@Override
	public void doGet(final HttpServletRequest request,final HttpServletResponse response) throws ServletException, IOException {
		final PrintWriter out = response.getWriter();
        String responsestring = "NO ACTION FOUND";
        final String action = request.getParameter("action");
        response.setContentType(CONTENT_TYPE);
        if (action != null && action.equalsIgnoreCase("refresh")) {
        	final String mode = request.getParameter("mode");
        	switch (mode) {
			case "bufferstatus":
				logger.info("Attempt to refresh buffer status [ " + ReceiverUtils.getCurrentDateTime() + " ] ");
				String bufferid = request.getParameter("bufferid");
                String bufferstatus = request.getParameter("status");
                BufferRepository.getInstance().setBufferStatus(bufferid, bufferstatus);
                responsestring = "buffer status " + bufferstatus.toUpperCase() + " set successfully";
				break;
			case "rt":	
				logger.info("Attempt to real time transaction status [ " + ReceiverUtils.getCurrentDateTime() + " ] ");
				Constants.IS_RT_ENABLED = Boolean.valueOf(request.getParameter("status"));
                responsestring = "Real time picker status set to " + Boolean.parseBoolean(request.getParameter("status"));
                break;
			case "brd":	
				logger.info("Attempt to real time transaction status [ " + ReceiverUtils.getCurrentDateTime() + " ] ");
				Constants.IS_BRD_ENABLED = Boolean.valueOf(request.getParameter("status"));
                responsestring = "Broadcast picker status set to " + Boolean.parseBoolean(request.getParameter("status"));
                break;
			case "recVendor":	
				logger.info("Attempt to refresh vendor status [ " + ReceiverUtils.getCurrentDateTime() + " ] ");
				VendorRepository.getInstance().reloadRepository();
				responsestring = "Vendor repository has been refreshed successfully";
                break;
			case "division":	
				logger.info("Attempt to refresh division status [ " + ReceiverUtils.getCurrentDateTime() + " ] ");
				DivisionRepository.getInstance().reloadRepository();
				responsestring = "Divison repository has been refreshed successfully";
                break;
			case "template":
				logger.info("Attempt to template [ " + ReceiverUtils.getCurrentDateTime() + " ] ");
				TemplateRepository.getInstance().reloadRepository();
				responsestring = "Template repository successfully refreshed";
				break;
			case "sender":
				logger.info("Attempt to sender [ " + ReceiverUtils.getCurrentDateTime() + " ] ");
				SenderRepository.getInstance().reloadRepository();
				responsestring = "Sender repository successfully refreshed";
				break;	
			default:
				break;
			}
        }
        out.write(responsestring);
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	public void destroy() {
	}

}
