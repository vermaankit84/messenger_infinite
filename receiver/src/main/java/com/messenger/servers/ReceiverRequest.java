package com.messenger.servers;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import com.messenger.constants.Constants;
import com.messenger.events.RequestEventManager;
import com.messenger.events.SuccessEventManager;
import com.messenger.httphandler.HttpClientManager;
import com.messenger.pool.ListnerThreadPool;
import com.messenger.pool.RecThreadPool;
import com.messenger.pool.ScheduledPoolFactory;
import com.messenger.thread.BrdPicker;
import com.messenger.thread.RealTimePicker;
import com.messenger.thread.SchBrdThread;
import com.messenger.thread.UnSchBrdThread;
import com.messenger.utils.ReceiverUtils;
public final class ReceiverRequest {
	private static ReceiverRequest receiverRequest = null;
	private final Logger logger = Logger.getLogger(ReceiverRequest.class.getName());
	private final String mode = Constants.STR_MODE;
	
	private ReceiverRequest() {
	}

	public static synchronized ReceiverRequest getReceiverRequest() {
		if (receiverRequest == null) {
			receiverRequest = new ReceiverRequest();
		}
		return receiverRequest;
	}

	
	public void startServer() {		
		logger.info("Receiver application has been started at [ " + ReceiverUtils.getCurrentDateTime() + " ] mode [ " + mode + " ] Process ID [ " +  ManagementFactory.getRuntimeMXBean().getName().split("@")[0] + "  ] ");
		ScheduledPoolFactory.getScheduledThreadPool().getScheduledPool("requestEventManager").scheduleAtFixedRate(new RequestEventManager(), 0, 1, TimeUnit.MINUTES);
		if ("rt".equals(mode)) {
			logger.info("Loading real time receiver [ " + ReceiverUtils.getCurrentDateTime() + " ] ");
			ScheduledPoolFactory.getScheduledThreadPool().getScheduledPool("realTimePicker").scheduleAtFixedRate(new RealTimePicker(), 0, 250, TimeUnit.MILLISECONDS);
		}  
		if ("brd".equals(mode)) {
			logger.info("Loading brd receiver [ " + ReceiverUtils.getCurrentDateTime() + " ] ");
			ListnerThreadPool.getInstance().buildPool();
			ScheduledPoolFactory.getScheduledThreadPool().getScheduledPool("brdPicker").scheduleAtFixedRate(new BrdPicker(), 0, 250, TimeUnit.MILLISECONDS);
			ScheduledPoolFactory.getScheduledThreadPool().getScheduledPool("unSchBrdThread").scheduleAtFixedRate( new SchBrdThread(), 0, 3, TimeUnit.SECONDS);
			ScheduledPoolFactory.getScheduledThreadPool().getScheduledPool("schBrdThread").scheduleAtFixedRate(new UnSchBrdThread(), 0, 3, TimeUnit.SECONDS);
		} 
		
		if ("http".equals(mode)) {
			logger.info("Loading http receiver [ " + ReceiverUtils.getCurrentDateTime() + " ] ");
			ScheduledPoolFactory.getScheduledThreadPool().getScheduledPool("successEventManager").scheduleAtFixedRate(new SuccessEventManager(), 0, 1, TimeUnit.MINUTES);
			ListnerThreadPool.getInstance().buildPool();
			RecThreadPool.getInstance().buildPool();
		}
	}

	public void stopServer() throws IOException {
		logger.info("Receiver application has been stopped at [ " + ReceiverUtils.getCurrentDateTime() + " ] mode [ " + mode + " ] ");
		ScheduledPoolFactory.getScheduledThreadPool().finish();		
		if ("http".equals(mode)) {
			ListnerThreadPool.getInstance().poolShutdown();
			RecThreadPool.getInstance().poolShutdown();
			HttpClientManager.getInstance().closepoolconnection();
		}
		if ("brd".equals(mode)) {
			ListnerThreadPool.getInstance().poolShutdown();
		}
	}
}
