package com.messenger.repositories;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.messenger.db.DBConn;
import com.messenger.utils.ReceiverUtils;
import com.messenger.valueobjects.DivisionVO;


public class DivisionRepository {
	private Map<String, DivisionVO> mSubDivReposoitry = null;
	private static DivisionRepository mInstance = null;
	private final Logger logger = Logger.getLogger(DivisionRepository.class.getName());
	private DivisionRepository() {
		mSubDivReposoitry = new HashMap<>();
		loadRepository();
	}

	public static synchronized  DivisionRepository getInstance() {
		if (mInstance == null) {
			mInstance = new DivisionRepository();			
		}
		return mInstance;
	}

	private void loadRepository() {
		String query = "SELECT DIVISIONID,DIVISIONPWD FROM mess_division";
	
		Connection conn = null;
		Statement statment = null;
		ResultSet resultSet = null;
		List<String> result = null;
		try {
			conn = DBConn.getconnetion();
			statment = conn.createStatement();
			resultSet = statment.executeQuery(query);
			result = ReceiverUtils.buildVectorFromResultSet(resultSet);
		} catch (Exception exception) {
			logger.warn("Exception arises while getting division repository", exception);
		} finally {
			DBConn.releaseDbConnection(statment, resultSet, conn, null);
		}
		if (result != null && !result.isEmpty()) {
			mSubDivReposoitry.clear();
			for (int loop = 0; loop < result.size(); loop += 2) {
				String pDivisionID = result.get(loop).toString().trim();
				String pDivisionPwd = result.get(loop + 1).toString().trim();
				mSubDivReposoitry.put(pDivisionID, new DivisionVO(pDivisionID , pDivisionPwd));
			}
		}

	}

	public DivisionVO getDivisionDetails(String mDivisionId, String mPassword) {
		DivisionVO d = null;
		final DivisionVO divisionVO = mSubDivReposoitry.get(mDivisionId);
		if (divisionVO!=null && mPassword.equals(divisionVO.getPpassword())) {
			d = divisionVO;
		}
		return d;
	}
	
	public void reloadRepository() {
		loadRepository();
	}
}
