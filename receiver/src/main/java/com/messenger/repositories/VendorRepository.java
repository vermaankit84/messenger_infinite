package com.messenger.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.messenger.constants.VendorType;
import com.messenger.db.DBConn;
import com.messenger.utils.ReceiverUtils;
import com.messenger.valueobjects.VendorVO;


public class VendorRepository {

	private Map<Integer, VendorVO> mRepository = new HashMap<>();
	private static VendorRepository mInstance = null;
	private static Logger log = Logger.getLogger(VendorRepository.class.getName());
	private VendorRepository() {
		loadRepository();
	}

	public static synchronized  VendorRepository getInstance() {
		if (mInstance == null) {
			mInstance = new VendorRepository();			
		}
		return mInstance;
	}

	private void loadRepository() {
		String query = "select vendorid , vendorname , vendororigin , vendortype , vendorurl , vendorpriority  , vendorsupport , vendorstatus , vendorheader , vendorcredentials  from mess_vendor_master";
		Connection conn = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<String> result = null;
		try {
			conn = DBConn.getconnetion();
			statement = conn.prepareStatement(query);
			resultSet = statement.executeQuery(query);
			result = ReceiverUtils.buildVectorFromResultSet(resultSet);			
		} catch (Exception e) {
			log.warn("Exception arises while loading vendor repository ", e);
		} finally {
			DBConn.releaseDbConnection(null, resultSet, conn, statement);
		}
		if (result != null && !result.isEmpty()) {
			mRepository.clear();
			for (int loop = 0; loop < result.size(); loop = loop + 10) {
				final int vendorId = Integer.parseInt(result.get(loop + 0).trim());
				final String vendorname = result.get(loop + 1).trim();
				final String vendororigin = result.get(loop + 2).trim();
				final VendorType vendortype = VendorType.getVendorType(Integer.parseInt(result.get(loop + 3).trim()));
				final String vendorurl = result.get(loop + 4).trim();
				final int vendorpriority = Integer.parseInt(result.get(loop + 5).trim());
				final String vendorsupport = result.get(loop + 6).trim();
				final String vendorHeader = result.get(loop + 8).trim();
				final String vendorCred = result.get(loop + 9).trim();
				if ("1".equalsIgnoreCase(result.get(loop + 7).trim())) {
					mRepository.put(vendorId, new VendorVO(vendorId, vendorname, vendororigin, vendortype, vendorurl, vendorpriority, vendorsupport , vendorHeader , vendorCred));
				}			
			}
		}		
	}
	
	
	
	public List<Integer> sortByPriority(List<Integer> pVendor) {
		if (pVendor.size() == 1) {
			return pVendor;
		}
		return pVendor.stream().sorted((o1 , o2) -> getVendorPrority(o1).compareTo(getVendorPrority(o2))).collect(Collectors.toList());
	}

	public Integer getVendorPrority(int pVendorId) {
		return mRepository.get(pVendorId).getVendorpriority();
	}

	public String getVendorOrign(int vendorID) {
		return mRepository.get(vendorID).getVendororigin();
	}
	
	public String getVendoSupport(int vendorID) {
		return mRepository.get(vendorID).getVendorsupport();
	}

	public VendorVO getVendorVO(int vendorId) {
		return mRepository.get(vendorId);
	}
	public String getVendorName(int pVendorId) {
		return mRepository.get(pVendorId).getVendorname();
	}
	public String getVendorUrl(int pVendorId) {
		return mRepository.get(pVendorId).getVendorurl();
	}
	
	public boolean isvendorRunning(int vendorId) {
		return (mRepository.get(vendorId)!=null);
	}
	public List<Integer> getOtpSmsVendors() {
		return mRepository.keySet().stream().filter(v -> (VendorType.SMS_OTP == getVendorType(v)) || (VendorType.SMS_EMAIL_OTP == getVendorType(v))).collect(Collectors.toList());
	}
	

	public VendorType getVendorType(int vendorID) {
		return mRepository.get(vendorID).getVendortype();
	}
	
	public boolean isSmsSupportedVendor(int vendorId) {
		boolean isSmsSupported = false;
		final VendorVO vendorVO = mRepository.get(vendorId);
		if (vendorVO!=null) {
			isSmsSupported = vendorVO.getVendortype().toString().contains("SMS");
		}
		return isSmsSupported;
	}
	
	public boolean isEmailSupportedVendor(int vendorId) {
		boolean isSmsSupported = false;
		final VendorVO vendorVO = mRepository.get(vendorId);
		if (vendorVO!=null) {
			isSmsSupported = vendorVO.getVendortype().toString().contains("EMAIL");
		}
		return isSmsSupported;
	}
	
	
	public List<Integer> getVendorList(final Predicate<Integer> vendorFilter ,final List<Integer> prefVendorList) {
		final List<Integer> vendorList = mRepository.keySet().stream().filter(vendorFilter).collect(Collectors.toList());
		if (prefVendorList!=null && !prefVendorList.isEmpty() && vendorList!=null && !vendorList.isEmpty()) {
			return vendorList.stream().filter(vendorId -> !prefVendorList.contains(vendorId)).collect(Collectors.toList());		
		}
		return vendorList;
	}

	public Map<String, String> getHeaderDetails(int vendorId) {
		Map<String , String> headerDetails = null;
		final VendorVO vendorVO = mRepository.get(vendorId);
		if (vendorVO != null && vendorVO.getVendorHeader() != null && !vendorVO.getVendorHeader().isEmpty()) {
			headerDetails = new HashMap<>();
			headerDetails.put("Content-Type" , "application/x-www-form-urlencoded");
			final String [] hDetails = vendorVO.getVendorHeader().split("-");
			for (final String s : hDetails) {
				headerDetails.put(s.split(",")[0] , s.split(",")[1]);
			}
		}
		return headerDetails;
	}
	
	public String getCredentails(final int vendorId) {	
		return mRepository.get(vendorId).getVendorCred();
	}

	public void reloadRepository() {
		loadRepository();
	}
}
