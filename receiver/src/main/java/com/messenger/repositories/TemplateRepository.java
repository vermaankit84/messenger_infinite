package com.messenger.repositories;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.messenger.db.DBConn;
import com.messenger.utils.ReceiverUtils;
import com.messenger.valueobjects.TemplateVO;



public class TemplateRepository {

	private Map<String, List<TemplateVO>> mRepository = new Hashtable<>();
	private static TemplateRepository mInstance = null;
	private static Logger log = Logger.getLogger(TemplateRepository.class.getName());
	public TemplateRepository() {
		loadRepository();
	}

	public static synchronized  TemplateRepository getInstance() {
		if (mInstance == null) {
			mInstance = new TemplateRepository();
			
		}
		return mInstance;
	}

	private void loadRepository() {
		final String strQuery = "select md.DIVISIONID , mtm.TEMP_ID , mtm.TEMPLATE_NAME , mtm.TEMPLATE_DETAILS from mess_division md inner join mess_template_master mtm on mtm.DIVISIONID = md.DIVISIONID and mtm.TEMPLATE_IS_ACTIVE = 1";
		Connection conn = null;
		Statement stat = null;
		ResultSet rs = null;
		List<String> result = null;
		List<TemplateVO> templateVector = null;

		try {
			conn = DBConn.getconnetion();
			stat = conn.createStatement();
			rs = stat.executeQuery(strQuery);
			result = ReceiverUtils.buildVectorFromResultSet(rs);
		} catch (Exception ex) {
			log.warn(" ****************** EXCEPTION ARISES IN TEMPLATE REPOSITORY ****************** " , ex);
		} finally {
			DBConn.releaseDbConnection(stat, rs, conn, null);
		}
		if (result != null && !result.isEmpty()) {
			for (int i = 0; i < result.size(); i = i + 4) {
				mRepository.clear();
				final String divId = result.get(i);
				final String templateId = result.get(i + 1);
				final String tempName = result.get(i + 2);
				final String tempMsg = result.get(i + 3);
				TemplateVO tempVo = new TemplateVO(templateId,tempName, tempMsg);
				if(mRepository.containsKey(divId)) {
					List<TemplateVO> templateVOs =  mRepository.get(divId);
					templateVOs.add(tempVo);
					mRepository.put(divId, templateVOs);
				} else {
					if(templateVector == null){
						templateVector = new ArrayList<>();
					}
					templateVector.add(tempVo);
					mRepository.put(divId, templateVector);
				}			
			}
		}
	}

	public boolean istemplateMatched(String divID ,String messagetext) {
		long count = 0;
		if (mRepository != null && !mRepository.isEmpty()) {
			final List<TemplateVO> templateList = mRepository.get(divID);
			if (templateList!=null && !templateList.isEmpty()) {
				count = templateList.stream().filter(t -> Pattern.compile(t.getTemplateMessage().toUpperCase().
						replaceAll("<dynamic>".toUpperCase(), "\\\\s*\\\\w+\\\\s*")).matcher(messagetext.toUpperCase()).matches()).count();
			}			
		}		
		return count > 0;
	}

	public void reloadRepository() {
		loadRepository();
	}
}
