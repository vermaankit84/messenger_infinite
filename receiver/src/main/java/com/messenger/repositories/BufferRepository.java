package com.messenger.repositories;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.messenger.constants.ReceiverConstants;
import com.messenger.db.DBConn;
import com.messenger.utils.ReceiverUtils;
import com.messenger.valueobjects.BufferVO;


public class BufferRepository {

	private  Map<String, BufferVO> mRepository = null;
	private static BufferRepository mInstance = null;
	private int mNationalBufferCounter = 0;
	private static Logger log = Logger.getLogger(BufferRepository.class.getName());
	private BufferRepository() {
		mRepository = new HashMap<>();
		loadRepository();
	}

	public static synchronized  BufferRepository getInstance() {
		if (mInstance == null) {
			mInstance = new BufferRepository();
		}
		return mInstance;
	}

	private  void loadRepository() {
		String strSelectBuffer = "SELECT BUFFERID,BUFFERNAME,BUFFERTABLE,BUFFERSTATUS,BUFFERTYPE FROM mess_bufferconn_master WHERE BUFFERSTATUS = 1";
		List<String> result = null;
		Connection conn = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			conn = DBConn.getconnetion();
			statement = conn.createStatement();
			resultSet = statement.executeQuery(strSelectBuffer);
			result = ReceiverUtils.buildVectorFromResultSet(resultSet);
		} catch (Exception ex) {
			log.warn ("******** EXCEPTION ARISES WHILE GETTING RECORDS ***********" , ex);
		} finally {
			DBConn.releaseDbConnection(statement, resultSet, conn,null);
		}

		if (result != null && !result.isEmpty()) {
			BufferVO bufferVO = null;
			for (int loop = 0; loop < result.size(); loop = loop + 5) {
				String bufferId = result.get(loop + 0).trim();
				String bufferName = result.get(loop + 1).trim();
				String bufferTable = result.get(loop + 2).trim();
				String bufferStatus = result.get(loop + 3).trim();
				String bufferType = result.get(loop + 4).trim();
				bufferVO = new BufferVO(bufferName, bufferTable, bufferStatus,bufferType);
				mRepository.put(bufferId, bufferVO);
			}
		}
	}

	public void reloadRepository() {
		loadRepository();
	}

	public synchronized String getNationalBuffer() {
		String nationalbufferid = null;
		List<String> allbuffer = getAllNationalBuffers();
		if (allbuffer != null && !allbuffer.isEmpty()) {
			if (allbuffer.size() > mNationalBufferCounter) {
				nationalbufferid = allbuffer.get(mNationalBufferCounter);
				mNationalBufferCounter = mNationalBufferCounter + 1;
			} else {
				nationalbufferid = allbuffer.get(0);
				mNationalBufferCounter = 1;
			}
		}
	
		return nationalbufferid;
	}

	private List<String> getAllNationalBuffers() {
		return mRepository.keySet().stream().filter
				(v -> getBufferType(v).equalsIgnoreCase(ReceiverConstants.STR_MESSENGER_BUFFER_TYPE) && 
						getBufferStatus(v).equalsIgnoreCase(ReceiverConstants.STR_MESSENGER_ACTIVE_BUFFERS)).collect(Collectors.toList());			
	}

	public String getBufferType(String pBufferId) {
		String bufferDBPType = null;
		BufferVO bufferVO =  mRepository.get(pBufferId);
		if (bufferVO != null) {
			bufferDBPType = bufferVO.getBufferType();
		}
		return bufferDBPType;
	}

	public String getBufferStatus(String pBufferId) {
		String bufferDBstatus = null;
		BufferVO bufferVO =  mRepository.get(pBufferId);
		if (bufferVO != null) {
			bufferDBstatus = bufferVO.getBufferStatus();
		}
		return bufferDBstatus;
	}

	public String getBufferTable(String pBufferId) {
		String bufferDBtable = null;
		BufferVO bufferVO =  mRepository.get(pBufferId);
		if (bufferVO != null) {
			bufferDBtable = bufferVO.getBufferTable();
		}
		return bufferDBtable;
	}

	public String getBufferName(String pBufferId) {
		String bufferName = null;
		BufferVO bufferVO =  mRepository.get(pBufferId);
		if (bufferVO != null) {
			bufferName = bufferVO.getBufferName();
		}
		return bufferName;
	}

	public void setBufferStatus(String pBufferId, String pBufferStatus) {
		BufferVO bufferVO =  mRepository.get(pBufferId);
		if (bufferVO != null) {
			mRepository.put(pBufferId, new BufferVO(bufferVO.getBufferName(),bufferVO.getBufferTable(), pBufferStatus,bufferVO.getBufferType()));
		}
	}
}
