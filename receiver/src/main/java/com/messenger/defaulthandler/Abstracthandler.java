package com.messenger.defaulthandler;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.RejectedExecutionException;

import org.apache.log4j.Logger;

import com.messenger.constants.ErrorConstants;
import com.messenger.constants.ReceiverConstants;
import com.messenger.events.RequestEventManager;
import com.messenger.pool.RecThreadPool;
import com.messenger.repositories.DivisionRepository;
import com.messenger.thread.DefaultThread;
import com.messenger.utils.ReceiverUtils;


public abstract class Abstracthandler implements Callable<String> {
	
	private final Logger logger = Logger.getLogger(Abstracthandler.class.getName());

	public final String doProcess (final Map<String, String> mRequest) throws UnsupportedEncodingException {
 		final String mDivisionId = mRequest.get("dcode");
 		final String mPassword = mRequest.get("pwd");
 		final String mSender = mRequest.get("sender");
 		final String mDestination = mRequest.get("pno");
 		final String mMessageText = ReceiverUtils.decode(ReceiverUtils.encode(mRequest.get("msgtxt")));
 		final String mInternationalFlag = mRequest.get("intflag");
 		final String mMsgType = mRequest.get("msgtype");
 		final String mPriority = mRequest.get("priority");
 		final String dndFlag = mRequest.get("mDNDFlag");
 		final String directpush = mRequest.get("priority");
 		final String vendorOrigin = mRequest.get("vendororigin");
        final String mParam = "PASS=" + mPassword + "::DIVISIONID =" + mDivisionId + "::SENDER=" + mSender + "::MOBILE=" + mDestination + "::TEXT=" + mMessageText + ":: MInternational Flag" + mInternationalFlag;
		final String errorstr = checkDefaultParameters(mDivisionId, mPassword, mSender, mDestination, mMessageText,mInternationalFlag, mMsgType, dndFlag);

        if (!"OK".equalsIgnoreCase(errorstr)) {
        	logger.info("MESSENGER::ERROR::PUSH REQUEST HANDLER::REJECT [ " + errorstr + mParam + " ] ".toLowerCase());
            return errorstr;
        }
        
        String mResponseId = ReceiverUtils.getResponseId(mDivisionId);
		RequestEventManager.addRequest(mDivisionId, mDestination.split(",").length);
        try {
        	final Map<String, String> processparameters = getProcessParameters(mDivisionId ,mSender, mDestination, mMessageText , mInternationalFlag,mResponseId,mPriority, mMsgType , dndFlag , ReceiverUtils.getMessageId(mDivisionId) , directpush , vendorOrigin);
        	if(processparameters!=null && !processparameters.isEmpty()) {
        		RecThreadPool.getInstance().getPool().submit(new DefaultThread(processparameters));       		
        	} else  {
        		mResponseId = "error";
        	}
        } catch (RejectedExecutionException rej) {
        	logger.warn("Exception arises while processing messeges ",rej);
        	mResponseId = "error";
        }     
        return mResponseId;
    }	
   
	private Map<String, String> getProcessParameters(String mDivisionId, String mSender, String mDestination,String mMessageText, String mInternationalFlag, String mResponseId, String mPriority, String mMsgType,String mDNDFlag, String msgId, String directpush, String vendorOrigin) {
		Map<String, String> processTable = new HashMap<>();
		processTable.put("divid", mDivisionId);
		processTable.put("from", mSender);
		processTable.put("to", mDestination);
		processTable.put("text", mMessageText);
		processTable.put("intflag", mInternationalFlag);
		processTable.put("respid", mResponseId);
		processTable.put("directpush", mPriority == null ? ReceiverConstants.STR_DEFAULT_MSG_PRIORITY : mPriority);
		processTable.put("msgtype", mMsgType == null ? ReceiverConstants.STR_DEFAULT_MSGTYPE : mMsgType);
		processTable.put("mDNDFlag",mDNDFlag != null && !mDNDFlag.isEmpty() ? mDNDFlag : ReceiverConstants.STR_DEFAULT_DND_FLAG);
		processTable.put("msgid", msgId);
		processTable.put("priority",directpush != null && !directpush.isEmpty() ? directpush : ReceiverConstants.STR_DEFAULT_DIRECT_PUSH);
		processTable.put("dlrreq", ReceiverConstants.STR_DEFAULT_DELIVERY_FLAG);
		processTable.put("cid", ReceiverConstants.STR_DEFAULT_CONTENT_ID);
		processTable.put("vendorOrigin",vendorOrigin!=null && !vendorOrigin.trim().isEmpty() ? vendorOrigin.trim() : "");
		return processTable;
	}
    
	private String checkDefaultParameters(String mDivisionId, String mPassword, String mSender, String mDestination, String mMessageText, String mInternationalflag,String mMsgType , String mDndFlag ) {
        String responsestring = "OK";
        if (mDivisionId == null || "".equals(mDivisionId)) {
            return ErrorConstants.MESSENGER_ERROR_MISSING_DIVISIONID;
        }
        
        
        if (mPassword == null || "".equals(mPassword)) {
            return ErrorConstants.MESSENGER_ERROR_MISSING_PASSWORD;
        }
        
            		
		if (DivisionRepository.getInstance().getDivisionDetails(mDivisionId , mPassword)== null) {
			return ErrorConstants.MESSENGER_ERROR_AUTHENTICATION_FAILED;
		}    
		
		if (mMsgType != null && !"".equals(mMsgType) && !"E".equalsIgnoreCase(mMsgType) && !"S".equalsIgnoreCase(mMsgType)) {
            return ErrorConstants.MESSENGER_INVALID_MSG_TYPE;
        }
		
		if (mSender == null || "".equals(mSender)) {
            return ErrorConstants.MESSENGER_ERROR_MISSING_SENDER;
        }
        if (mDestination == null || "".equals(mDestination)) {
            return ErrorConstants.MESSENGER_ERROR_MISSING_MOBILENUMBER;
        }
        if (mMessageText == null || "".equals(mMessageText)) {
            return ErrorConstants.MESSENGER_ERROR_MISSING_MESSAGETEXT;
        }  
        if ("S".equalsIgnoreCase(mMsgType)) {
        	if (mInternationalflag == null || "".equalsIgnoreCase(mInternationalflag.trim())) {
    			return ErrorConstants.MESSENGER_ERROR_INTERNATIONAL_FLAG_NOT_FOUND;
    		}
    		if (!("1".equalsIgnoreCase(mInternationalflag)|| "2".equalsIgnoreCase(mInternationalflag))) {
    			return ErrorConstants.MESSENGER_ERROR_INTERNATIONAL_FLAG_NOT_VALID;
    		}
        }
        
        if ("E".equalsIgnoreCase(mMsgType) && mDndFlag!=null && !mDndFlag.trim().isEmpty()) {
        	return ErrorConstants.MESSENGER_DND_FLAG_NOT_WITH_EMAIL;
        }
        
        if ("S".equals(mMsgType) && !("1".equals(mDndFlag) || "2".equals(mDndFlag) || "3".equals(mDndFlag))) {
        	return ErrorConstants.MESSENGER_DND_FLAG_INVALID;
        }
        
        if ("S".equals(mMsgType) && !ReceiverUtils.isNumeric(mDestination)) {
        	return ErrorConstants.MESSENGER_INVALID_MSIDN_RECEIVED;
        }
        return responsestring;
    }

    
}
