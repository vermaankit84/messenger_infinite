package com.messenger.service;

import org.apache.log4j.Logger;

import com.messenger.httphandler.HttpClientManager;
import com.messenger.repositories.VendorRepository;
import com.messenger.response.ParseVendorResponse;
import com.messenger.utils.ReceiverUtils;

public class MessageService {
	private final Logger logger = Logger.getLogger(MessageService.class.getName());
	private final ParseVendorResponse parseVendorResponse = new ParseVendorResponse();
	public final String sendMessage(final int vendorID , final String mMessageText , final String mobile , final String mSenderId , final String mResponseId , final String mIntFlag , final String mMessageType) throws Exception {
		logger.info("Trying to send message via [ " + vendorID + " ] [ " + ReceiverUtils.getCurrentDateTime() + " ] ");
		String primaryUrl = VendorRepository.getInstance().getVendorUrl(vendorID);
		if (primaryUrl != null) {
			primaryUrl = primaryUrl.replaceAll("%TEXT",ReceiverUtils.decode(mMessageText)).replaceAll("%TO",ReceiverUtils.encode(mobile.trim())).replaceAll("%FROM",ReceiverUtils.encode(mSenderId)).replaceAll("%MSGID",ReceiverUtils.encode(mResponseId)).replaceAll("%INTFLAG", mIntFlag);
			primaryUrl = primaryUrl.replaceAll("%MSGTYPE", mMessageType).replaceAll("%25MSGID", mResponseId).replaceAll("%25TO", mobile.trim());			
		}
		
		final String mode = VendorRepository.getInstance().getVendoSupport(vendorID);
		String strResponse = null;
		if (mode != null && !mode.isEmpty()) {
			if ("GET".equalsIgnoreCase(mode)) {
				strResponse = HttpClientManager.getInstance().generateGetResponse(primaryUrl);
			} else {
				strResponse = HttpClientManager.getInstance().generatePostResponse(primaryUrl.split("-")[0], 
						VendorRepository.getInstance().getHeaderDetails(vendorID),ReceiverUtils.getNameValuePair(primaryUrl.split("-")[1].split("\\&")) , 
						VendorRepository.getInstance().getCredentails(vendorID));
			}
		}
		if (strResponse!=null && !strResponse.isEmpty()) {
			return parseVendorResponse.ParseResponseId(strResponse, vendorID);
		}
		return strResponse;
	}
}
