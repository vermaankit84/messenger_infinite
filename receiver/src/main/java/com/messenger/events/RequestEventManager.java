package com.messenger.events;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.messenger.db.DBConn;
import com.messenger.valueobjects.RequestEventVO;


public class RequestEventManager implements Runnable {
	private final Logger logger = Logger.getLogger(RequestEventManager.class.getName());
	private static Hashtable<String, RequestEventVO> mRepository = new Hashtable<>();
	public RequestEventManager() {
		
	}
	
	@Override
	public void run() {
		processMessages();
	}

	private void processMessages() {
		Enumeration<String> hashenum = mRepository.keys();
		Connection conn = null;
		PreparedStatement prep = null;
		int[] rowsUpdated = null;
		String strQuery = "INSERT INTO MESS_EVTMGR_REQREC VALUES ( ? , ? , ? , ? , ? ,? ,?)";		 
		try {			
			conn = DBConn.getconnetion();
		   	prep = conn.prepareStatement(strQuery);
			while (hashenum.hasMoreElements()) {
				String divisionId = hashenum.nextElement();
				if (divisionId != null) {
					RequestEventVO requestEventVO = mRepository.get(divisionId);
					int noOfMsg = requestEventVO.getRequestCount();
					final Calendar cal = Calendar.getInstance();
					prep.setString(1, divisionId);
					prep.setInt(2, cal.get(Calendar.DATE));
					prep.setInt(3, cal.get(Calendar.MONTH) + 1);
					prep.setInt(4, cal.get(Calendar.YEAR));
					prep.setInt(5, cal.get(Calendar.HOUR_OF_DAY));
					prep.setInt(6, cal.get(Calendar.MINUTE));
					prep.setInt(7, noOfMsg);

					prep.addBatch();
					mRepository.remove(divisionId);
				}
			}
			rowsUpdated = prep.executeBatch();
			 if (rowsUpdated != null && rowsUpdated.length > 0) {
				logger.info("-------- REQUEST EVENT NO OF MESSAGES OBTAINED ----------->" + rowsUpdated.length);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			DBConn.releaseDbConnection(null, null, conn, prep);;
		}
	}

	public static synchronized  void addRequest(String pDivisionId, int pTotalRequests) {
		if (mRepository.containsKey(pDivisionId)) {
			RequestEventVO objEntity = mRepository.get(pDivisionId);
			objEntity.setRequestCount(objEntity.getRequestCount()+ pTotalRequests);
			mRepository.put(pDivisionId,objEntity);
		} else {
			mRepository.put(pDivisionId , new RequestEventVO(pTotalRequests));
		}
	}

	public static int getTotalCountInMin(String pDivisionId) {
		int pRequestCount = 0;
		if (mRepository.containsKey(pDivisionId)) {
			pRequestCount = (mRepository.get(pDivisionId)).getRequestCount();
		}
		return pRequestCount;
	}
}
