package com.messenger.events;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.messenger.db.DBConn;
import com.messenger.valueobjects.SuccessEventVO;

public class SuccessEventManager extends Thread {
	private static Hashtable<String, SuccessEventVO> mRepository = new Hashtable<String, SuccessEventVO>();
	private static final Logger logger = Logger.getLogger(SuccessEventManager.class.getName());
	
	@Override
	public void run() {
		processMessages();
	}
	

	private void processMessages() {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		String strQuery = "INSERT INTO mess_evtmgr_vendorsuccess VALUES ( ? , ? , ? , ? , ? , ? , ?  )";
	    Enumeration<String> hashenum = mRepository.keys();
		try {
			conn = DBConn.getconnetion();
			preparedStatement = conn.prepareStatement(strQuery);
			while (hashenum.hasMoreElements()) {
				String key = hashenum.nextElement();
				if (key != null) {
					final SuccessEventVO successEventVO = mRepository.get(key);
					final int pPrimaryCount = successEventVO.getPrimaryCount();
					final Calendar cal = Calendar.getInstance();
					preparedStatement.setString(1, key);
					preparedStatement.setString(2, String.valueOf(cal.get(Calendar.DATE)));
					preparedStatement.setString(3, String.valueOf(cal.get(Calendar.MONTH) + 1));
					preparedStatement.setString(4, String.valueOf(cal.get(Calendar.YEAR)));
					preparedStatement.setString(5, String.valueOf(cal.get(Calendar.HOUR_OF_DAY)));
					preparedStatement.setString(6, String.valueOf(cal.get(Calendar.MINUTE)));
					preparedStatement.setInt(7, pPrimaryCount);
					mRepository.remove(key);
					preparedStatement.addBatch();       
				}
			}
			int[] noOfRows = preparedStatement.executeBatch();
			if (noOfRows != null && noOfRows.length > 0) {
				logger.info(" ------------ ROWS UPDATED IN SUCCESS EVENT MANAGER ----> " + noOfRows.length);
			}
			
		} catch (Exception ex) {
			logger.warn ("EXCEPTION ARISES IN SUCCESS EVENT MANAGER "  , ex);
		} finally {
			DBConn.releaseDbConnection(null, null, conn, preparedStatement);
		}
		
	}

	public static synchronized  void addRequest(String pVendorId,int pPrimaryCount, int pSecondaryCount) {
		if (mRepository.containsKey(pVendorId)) {
			SuccessEventVO objEntity = mRepository.get(pVendorId);
			if (pPrimaryCount > 0){
				objEntity.setPrimaryCount(objEntity.getPrimaryCount() + pPrimaryCount);
			}
			if (pSecondaryCount > 0){
				objEntity.setSecondaryCount(objEntity.getSecondaryCount() + pSecondaryCount);
			}
			mRepository.put(pVendorId, objEntity);
		} else {
			mRepository.put(pVendorId, new SuccessEventVO(pVendorId, pPrimaryCount, pSecondaryCount));
		}
	}
}
