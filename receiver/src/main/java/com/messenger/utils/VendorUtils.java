package com.messenger.utils;

public class VendorUtils {
	
	private VendorUtils() {}
	
	public static String parseVendorResponse(String vendorResponse, String vendorOrign) {
		String response = null;
		if (vendorResponse!=null && !vendorResponse.isEmpty()) {
			switch (vendorOrign) {
			case "ACL":
				response = vendorResponse.split(":")[1];
				break;
			case "Twilito" :
				String [] strData = vendorResponse.split("\\,");
		        for (String s: strData) {
		            if (s.contains("sid")) {
		                String sid  = s.split("\\:")[1];
		                response = sid.substring(2 , sid.length() - 1);
		                break;
		            }
		        }
				break;
			default:
				throw new IllegalArgumentException("This vendor origin is not supported [ " + vendorOrign + " ] ");
			}
		}		
		return response;
	}

}
