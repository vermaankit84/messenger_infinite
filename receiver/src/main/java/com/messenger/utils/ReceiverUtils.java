package com.messenger.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.messenger.defaulthandler.Abstracthandler;
import com.messenger.handlers.PushRequestHandler;
import com.messenger.repositories.BufferRepository;
import com.messenger.repositories.VendorRepository;
import com.messenger.service.MessageService;

@SuppressWarnings("unchecked")
public final class ReceiverUtils {

	private static AtomicInteger mCounter = new AtomicInteger(0);
	
	private ReceiverUtils() {
	}

	public static synchronized  String getMessageId(String pEntId) {
		return "msgid-" + pEntId + "-" + System.currentTimeMillis() + "-" + getIncrementedCounter() + "-" + "01" + "01";
	}

	public static synchronized  String getResponseId(String pDivId) {
		return "APP-" + pDivId + "-" + System.currentTimeMillis() + "-" + getIncrementedCounter() + "-" + "01" + "01";
	}
	
	public static String getBufferTable() {
		String bufferid = BufferRepository.getInstance().getNationalBuffer();
		if (bufferid == null) {
			bufferid = "1";
		}
		return BufferRepository.getInstance().getBufferTable(bufferid);
	}
	
	
	private static String getIncrementedCounter() {
		mCounter.incrementAndGet();
		if (mCounter.intValue() > 9999) {
			mCounter = new AtomicInteger(1);
		}
		return String.valueOf(mCounter.intValue());
	}
	
	public static boolean isNumeric(String pStr) {
		boolean isNumeric = true;
		try {
			Long.parseLong(pStr.trim());
			isNumeric = true;
		} catch (NumberFormatException ex) {
			isNumeric = false;
		} catch (Exception ex) {
			isNumeric = false;
		}
		return isNumeric;
	}
	
	public static String getCurrentDateTime() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new GregorianCalendar().getTime());
	}
	
	public static List<String> buildVectorFromResultSet(ResultSet resultSet) throws SQLException {
		List<String> resultVectorSet = null;
		ResultSetMetaData rsmDataObject = resultSet.getMetaData();
		while (resultSet.next()) {
			if (resultVectorSet == null) {
				resultVectorSet = new ArrayList<String>();
			}
			for (int i = 0; i < rsmDataObject.getColumnCount(); i++) {
				resultVectorSet.add(resultSet.getString(i + 1));
			}
		}

		return resultVectorSet;
	}
	
	public static Map<String, String> getRequestParameters(HttpServletRequest request) {
		try {
			Map<String, String> requestParams = null;
			Enumeration<String> paramNames = request.getParameterNames();
			while (paramNames.hasMoreElements()) {
				String key = paramNames.nextElement();
				String value = request.getParameter(key);
				if (key.equalsIgnoreCase("text") && request.getParameter("charset") != null) {
					try {
						String value1 = new String(value.getBytes(), request.getParameter("charset"));
						value = value1;
					} catch (Exception ex) {
					}
				}
				if (requestParams == null) {
					requestParams = new Hashtable<String, String>();
				}
				requestParams.put(key, value);
			}
			return requestParams;
		} catch (Exception ex1) {
			return null;
		}
	}
	
	public static Abstracthandler getHandler(final HttpServletRequest request, Map<String, String> reqRec) {
		Abstracthandler handler = null;
		String strRequest = request.getRequestURI().split("/")[2];
		if("messengerListner".equalsIgnoreCase(strRequest)) {
			handler =  new PushRequestHandler(reqRec);
		} 	
		return handler;
	}
	
	public static String encode(final String msgtext) throws UnsupportedEncodingException {
		if (msgtext == null) {
			return null;
		}
		return URLEncoder.encode(msgtext , "ISO-8859-1");
	}
	
	public static String decode(final String msgtext) throws UnsupportedEncodingException {
		if (msgtext == null) {
			return null;
		}
		return URLDecoder.decode(msgtext , "ISO-8859-1");
	}

	public static String[] sendDirectPush(String mobile, String mResponseId, String mSenderId,  String mContentType, String mPriority, String mMessageText, String mDepartmentCode,String mIntFlag, String string, String mMessageType, List<Integer> vendorList , Logger looger) throws Exception {
		final MessageService messageService = new MessageService();
		String[] response = null;
		final List<Integer> sortedVendors = VendorRepository.getInstance().sortByPriority(vendorList);
		looger.info("Vendors obtained [ " +  sortedVendors + " ] for Message Id [ " + mResponseId + " ] msisdn [ " + mobile + " ] ");
		for (final int vendorID : sortedVendors) {
				String vendorResponse = messageService.sendMessage(vendorID, mMessageText, mobile, mSenderId, mResponseId, mIntFlag, mMessageType);
				if (vendorResponse!=null && !vendorResponse.isEmpty()) {
					response = new String[2];
					response[0] = vendorResponse;
					response[1] = String.valueOf(vendorID);
					break;
				}
		}
		return response;
	}
	
	public static Map<String, String> getNameValuePair(String[] str) {
		return Arrays.stream(str).collect(Collectors.toMap(new Function<String, String>() {
			@Override
			public String apply(String s) {
				return s.split("\\=")[0];
			}
		}, new Function<String, String>() {
			@Override
			public String apply(String s) {
				return s.split("\\=")[1];
			}
		}));
	}
}
