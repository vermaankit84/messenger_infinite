package com.messenger.thread;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.apache.log4j.Logger;

import com.messenger.db.DBConn;

public final class SchedulerThread implements Runnable {

	private int maxRowCount = 2;
    private String brdId = null;
    private int pTotalRequests = 0;
    private final Logger logger = Logger.getLogger(SchedulerThread.class.getName());
    
	public SchedulerThread(final String brdId,final int pTotalRequests) {
		this.brdId = brdId;
		this.pTotalRequests = pTotalRequests;
	}
	@Override
	public final void run() {
		try {
            doProcess();
        } catch (Exception ex) {
        	logger.warn("Exception arises in scheduler thread " , ex);
        }
	}
	
	private void doProcess() {
		if (execute(brdId, pTotalRequests, maxRowCount)) {
			deletefromtemp(brdId);
			updateStatus(brdId, "2");
		} else {
			updateStatus(brdId, "0");
		}
	}

    private boolean deletefromtemp(String brdId) {
        boolean isUpdateSucess = false;
        String delQuery = "delete from mess_brd_temp_table where brdid = ?";
        Connection conn = null;
        PreparedStatement prep = null;
        try {
            conn = DBConn.getconnetion();
            prep = conn.prepareStatement(delQuery);
            prep.setString(1, brdId);
            isUpdateSucess = prep.executeUpdate() > 0 ;
        } catch (Exception ex) {
        	logger.warn("Exception arises in scheduler thread while deleting data from temp table " , ex);
        } finally {
            DBConn.releaseDbConnection(null, null, conn, prep);
        }

        return isUpdateSucess;
    }

    private boolean updateStatus(String brdId , String status) {

        boolean isUpdateSucess = false;
        Connection conn = null;
        PreparedStatement prep = null;
        String strQuery = "update mess_brd_master set status = ? where brdid = ?";
        try {
            conn = DBConn.getconnetion();
            prep = conn.prepareStatement(strQuery);
            prep.setString(1, "2");
            prep.setString(2, brdId);
            isUpdateSucess = prep.executeUpdate() > 0 ? true : false;
        } catch (Exception ex) {
        	logger.warn("Exception arises in scheduler thread while updating status " , ex);
        } finally {
        	DBConn.releaseDbConnection(null, null, conn, prep);
        }
       
        return isUpdateSucess;
    }    
    
	public boolean execute(String brdId, int broadcastcount, int maxrowcount) {
		boolean isUpdateSucess = false;
		Connection conn = null;
		PreparedStatement prep = null;
		String strQuery = "INSERT INTO brd_trans (MESSAGEID , PHONENUMBER , MESSAGEPRIORITY , STATUS , MESSAGETEXT , DEPARTMENTCODE , SENDERNAME , "
				+ "INTERNATIONALFLAG , MESSAGETYPE , CONTENTTYPE , RETRYATTEMPT , SUBMITTEDTIME , SIURL , DNDFLAG , VENDORORIGIN) "
				+ "select x.msgid , x.msisdn , x.msgpri , x.sucessStatus , x.msgtext , x.depcode , x.senderid , x.intflag ,x.msgType , x.contenttype , x.retryattempt ,"
				+ " x.submittedtime , x.siurl , x.dndflag , x.vendorOrgin from (select c.*  from ( select   b.MSGID  as msgid , b.destination as msisdn , "
				+ " b.messagepriority as msgpri,'0' as sucessStatus, b.messagetext as msgtext,a.DEPCODE as depcode,a.SENDERNAME as senderid,"
				+ " b.intflag as intflag,b.msgtype as msgtype, '1' as contenttype ,'0' as retryattempt , CURRENT_TIMESTAMP() as submittedtime ,"
				+ "'NA' as siurl , '0' as dndflag , a.VENDORORIGIN as vendorOrgin from mess_brd_master a , "
				+ " mess_brd_temp_table b where a.BRDID = b.BRDID and a.STATUS = ? and b.brdid = ? LIMIT ?,? ) c ) x".toLowerCase();
		try {
			conn = DBConn.getconnetion();
			prep = conn.prepareStatement(strQuery);
			prep.setString(1, "3");
			prep.setString(2, brdId);

			for (int i = 0; i < broadcastcount; i += maxrowcount) {
				prep.setInt(3, i);
				prep.setInt(4, maxrowcount);
				isUpdateSucess = prep.executeUpdate() > 0;
			}
		} catch (Exception ex) {
			logger.warn("Exception arises in scheduler thread while migrating data to transaction table " , ex);
		} finally {
			DBConn.releaseDbConnection(null, null, conn, prep);
		}
		return isUpdateSucess;
	}

}
