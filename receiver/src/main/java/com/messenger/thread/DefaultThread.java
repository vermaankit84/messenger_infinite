package com.messenger.thread;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.messenger.constants.ReceiverConstants;
import com.messenger.events.SuccessEventManager;
import com.messenger.queries.QueryBuilder;
import com.messenger.repositories.SenderRepository;
import com.messenger.repositories.TemplateRepository;
import com.messenger.repositories.VendorRepository;
import com.messenger.utils.ReceiverUtils;

public class DefaultThread implements Runnable {

    private Map<String, String> mRequest = null;
    private final Logger logger = Logger.getLogger(DefaultThread.class.getName());
    public DefaultThread(Map<String, String> pRequest) {
        this.mRequest = pRequest;
    }

    @Override
    public void run() {
        try {
            doProcess();
        } catch (Exception ex) {
        	logger.warn("Exception arises while serving for request [ " + mRequest + " ] ", ex);
        }
    }
    
    private void doProcess() throws Exception {
    	final String mDepartmentCode = mRequest.get("divid");
    	final String mContentType = mRequest.get("cid");
    	final String mSenderId = mRequest.get("from");
    	final String mPhoneNumber = mRequest.get("to");
    	final String mMessageText = mRequest.get("text");
    	final String mIntFlag = mRequest.get("intflag");
    	final String mMessageId = mRequest.get("msgid");
    	final String mDeliveryFlag = mRequest.get("dlrreq");
    	final String mAppReceivedDateTime = mRequest.get("reqtime");
    	final String mResponseId = mRequest.get("respid");
    	final String mMessageType = mRequest.get("msgtype").toUpperCase();
    	final String mDNDFlag = mRequest.get("mDNDFlag");
    	final String mPriority = mRequest.get("priority");
    	final String mSMSCount = String.valueOf(mMessageText.length());
    	final String vendororigin = mRequest.get("vendorOrigin");
    	final String []msisdns = mPhoneNumber.split(",");
        for (int loop = 0; loop < msisdns.length; loop++) {
        	final String mobile = msisdns[loop];
        	makeProcessLog(mMessageId, mAppReceivedDateTime, mDeliveryFlag, mContentType, mobile, mPriority, mMessageText, mDepartmentCode, mIntFlag, mMessageType, mSenderId,  mDNDFlag ,  mResponseId ,  mIntFlag,vendororigin);
            makeMisRecLog(mMessageId, mResponseId, mDepartmentCode, mSenderId, mAppReceivedDateTime, mDeliveryFlag, mMessageType, mobile, mPriority, mMessageText, mSMSCount, mDNDFlag,  mIntFlag , vendororigin);
        }
    }

    private void makeMisSubLog(String mMessageId, String pMobile, String mStatus, String mStatusDesc , String vendorResponse , String vendorid , String submitTime) {
        QueryBuilder.insertIntoMisSubLog(mMessageId, pMobile, mStatus, mStatusDesc , vendorResponse , vendorid , submitTime);
    }

    private void makeMisRecLog(String mMessageId, String mResponseId, String mDepartmentCode, String mSenderId, String mAppReceivedDateTime, String mDeliveryFlag, String mMessageType, String pMobile, String mPriority, String mMessageText, String mSMSCount, String mDNDFlag, String intFlag , String vendororigin ) {
        QueryBuilder.insertInMisRecLog(mMessageId, mResponseId, mDepartmentCode,mSenderId, mAppReceivedDateTime, mDeliveryFlag, mMessageType, pMobile, mPriority, mMessageText, mSMSCount, mDNDFlag, intFlag , vendororigin);
    }

    private void makeProcessLog(String mMessageId, String mAppReceivedDateTime, String mDeliveryFlag, String mContentType, String pMobile, String mPriority, String mMessageText, String mDepartmentCode, String mIntFlag, String mMessageType, String mSenderId, String mDNDFlag , String mResponseId , String intFlag , String vendorOrigin) throws Exception {
    	String[] response = null;
    	if("1".equalsIgnoreCase(mPriority) && "1".equals(mDNDFlag) && "S".equalsIgnoreCase(mMessageType)){
    		if(!TemplateRepository.getInstance().istemplateMatched(mDepartmentCode, mMessageText)) {
    			makeMisSubLog(mMessageId, pMobile, "R", "Invalid template obtained", null, null, ReceiverUtils.getCurrentDateTime());
    			logger.info( new StringBuilder().append("MESSENGER::ERROR::REQUEST PROCESSOR:::REJECT ".toLowerCase()).append(pMobile).append(":::").append("Invalid template obtained").toString().toLowerCase());
    			return;
    		}
    		if(!SenderRepository.getInstance().isValidSender(mDepartmentCode, mSenderId)) {
    			makeMisSubLog(mMessageId, pMobile, "R", "Invalid Sender obtained", null, null, ReceiverUtils.getCurrentDateTime());
    			logger.info(new StringBuilder().append("MESSENGER::ERROR::REQUEST PROCESSOR:::REJECT ".toLowerCase()).append(pMobile).append(":::").append("Invalid Sender obtained").toString().toLowerCase());
    			return;
    		}
    		List<Integer> vendorList =  VendorRepository.getInstance().getOtpSmsVendors();
    		if(vendorList!=null && !vendorList.isEmpty()) {
    			if (vendorOrigin!=null && !StringUtils.isEmpty(vendorOrigin)) {
    				vendorList = vendorList.stream().filter(v -> vendorOrigin.equalsIgnoreCase(VendorRepository.getInstance().getVendorOrign(v))).collect(Collectors.toList());
    			}
    			logger.info("Vendor List Obtained For Message Id [ " + mMessageId + " ] Mobile Number [ " + pMobile + " ] Vendor List [ " + vendorList + "  ] ");
    			response = ReceiverUtils.sendDirectPush(pMobile, mResponseId, mSenderId, mContentType, mPriority, mMessageText, mDepartmentCode, mIntFlag, "NA", mMessageType , vendorList , logger);
    			logger.info("Messenger response obtained from OTP URL --------->" + ((response != null && response.length == 2) ? response[0] : "no response obtained" ));
        		if (response != null && response.length == 2) {
        			makeMisSubLog(mMessageId, pMobile, ReceiverConstants.STR_MESSENGER_SUBMITTED_SMSC, ReceiverConstants.STR_MESSENGER_SUBMITTED_TO_SMSC , response[0] , response[1] , ReceiverUtils.getCurrentDateTime());
        			SuccessEventManager.addRequest(response[1], 1, 0);
        			return ;
        		}
    		}
        }
    	
    	if(response == null) {
    		QueryBuilder.insertInBufferLog(mMessageId, mAppReceivedDateTime, mDeliveryFlag, mContentType, pMobile, mPriority, mMessageText, mDepartmentCode, mIntFlag, mMessageType, ReceiverUtils.getBufferTable(), mSenderId,mDNDFlag);
    	}
    }
}
