package com.messenger.thread;

import com.messenger.template.PickerThreads;


public class BrdPicker extends PickerThreads {
	
	public BrdPicker() {
		super("mess_brd_trans" , "BRD" , 251);	
	}

	@Override
	public final void run() {
		executeProcess();
	}
}