package com.messenger.thread;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.apache.log4j.Logger;
import com.messenger.db.DBConn;
import com.messenger.template.BrdTemplate;


public final class SchBrdThread extends BrdTemplate {
	private final Logger logger = Logger.getLogger(BrdTemplate.class.getName());
	@Override
	public final void doProcess() {
		final int rowsUpdated = markUpdate();
		if (rowsUpdated > 0) {
			logger.info("No of rows found updated [ " + rowsUpdated + " ] ");
		} 
	}
	
	private int markUpdate() {
        int noOfRows = 0;
        Connection conn = null;
        PreparedStatement prep = null;
        String strQuery = "update mess_brd_master set status = ? where timestampdiff(second,scheduled_date, CURRENT_TIMESTAMP()) >=? and status = ?";
        try {
            conn = DBConn.getconnetion();
            prep = conn.prepareStatement(strQuery);
            prep.setString(1, "0");
            prep.setInt(2, 0);
            prep.setString(3, "1");
            noOfRows = prep.executeUpdate();
        } catch (Exception e) {
        	logger.warn("Mark update in messenger broadcast master failed" , e);
        } finally {
        	DBConn.releaseDbConnection(null, null, conn, prep);
        }
        return noOfRows;
    }

}
