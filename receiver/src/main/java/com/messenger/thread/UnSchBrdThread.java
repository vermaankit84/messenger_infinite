package com.messenger.thread;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.apache.log4j.Logger;

import com.messenger.db.DBConn;
import com.messenger.pool.ListnerThreadPool;
import com.messenger.template.BrdTemplate;
import com.messenger.utils.ReceiverUtils;

public class UnSchBrdThread extends BrdTemplate {

	private Logger logger = Logger.getLogger(this.getClass().getName());
	private static final  int MAX_ROW_COUNT = 10;
	
	@Override
	public final void doProcess() {
		final List<String> brdDeatils = buildDetails();
        if (brdDeatils != null && !brdDeatils.isEmpty()) {
            for (int i = 0; i < brdDeatils.size(); i = i + 3) {
            	final String brdId = brdDeatils.get(i);
                final String depCode = brdDeatils.get(i + 1);
                final int pTotalRequests = Integer.parseInt(brdDeatils.get(i + 2));
                logger.info("Details obtained for processing [ " + brdId + " ] Department Code [ " + depCode + " ] Total Request [ " + pTotalRequests + " ] ");
                if (markPick(brdId)) {
                	ListnerThreadPool.getInstance().getPool().submit(new SchedulerThread(brdId, pTotalRequests));
                } else {
					logger.info("Messenger update failed will be picked by next thread ");
                }
            }
        } 		
	}
	
	private  List<String> buildDetails() {
        List<String> brdDeatils = null;
        Connection conn = null;
        PreparedStatement prep = null;
        ResultSet rs = null;
        String strQuery = "select brdid , depcode ,brd_count  from mess_brd_master where STATUS = ? LIMIT ?";
        try {
            conn = DBConn.getconnetion();
            prep = conn.prepareStatement(strQuery);
            prep.setString(1, "0");
            prep.setInt(2, MAX_ROW_COUNT);
			rs = prep.executeQuery();
            brdDeatils = ReceiverUtils.buildVectorFromResultSet(rs);
        } catch (Exception e) {
        	logger.warn("Exception arises while building set for scheduling" , e);
        } finally {
            DBConn.releaseDbConnection(null, rs, conn, prep);
        }

        return brdDeatils;
    }

	private boolean markPick(String brdID) {
		boolean isUpdateSucess = false;
		Connection conn = null;
		PreparedStatement prep = null;
		String strQuery = "update mess_brd_master set status = ? where status = ? and brdid = ?";
		try {
			conn = DBConn.getconnetion();
			prep = conn.prepareStatement(strQuery);
			prep.setString(1, "3");
			prep.setString(2, "0");
			prep.setString(3, brdID);
			isUpdateSucess = prep.executeUpdate() > 0;
		} catch (Exception e) {
			logger.warn("Mark update in Broadcast details failed " , e);
		} finally {
			DBConn.releaseDbConnection(null, null, conn, prep);
		}
		return isUpdateSucess;
	}
}
