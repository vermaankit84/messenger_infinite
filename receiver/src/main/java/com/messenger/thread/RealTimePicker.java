package com.messenger.thread;

import com.messenger.constants.Constants;
import com.messenger.template.PickerThreads;

public class RealTimePicker extends PickerThreads {
	
	public RealTimePicker() {
		super("real_time_transaction" , "RT" , 251);
	}

	@Override
	public void run() {
		if (Constants.IS_RT_ENABLED) {
			executeProcess();			
		}
	}
}