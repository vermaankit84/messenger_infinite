package com.messenger.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class RecThreadPool {

	private int poolsize = 100;
	private ExecutorService pool = null;
	private static RecThreadPool instance = null;
	private boolean isPoolBuild = false;
	private RecThreadPool() {
	}

	public void buildPool() {
		if (isPoolBuild) {
			throw new IllegalAccessError("Rec thread pool already build will not be nuild up again ");
		}
		isPoolBuild = true;
		pool = Executors.newFixedThreadPool(poolsize);
	}

	public static synchronized RecThreadPool getInstance() {
		if (instance == null) {
			instance = new RecThreadPool();
		}
		return instance;
	}

	public ExecutorService getPool() {
		return pool;
	}

	public void poolShutdown() {
		try {
			if (pool != null) {
				pool.awaitTermination(1, TimeUnit.SECONDS);
				pool.shutdown();
				if (!pool.isShutdown()) {
					pool.shutdownNow();
				}
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
