package com.messenger.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class ListnerThreadPool {

	private ExecutorService pool = null;
	private static ListnerThreadPool instance = null;
	private boolean isPoolBuild = false;
	private ListnerThreadPool() {
		
	}

	public static synchronized ListnerThreadPool getInstance() {
		if (instance == null) {
			instance = new ListnerThreadPool();
		}
		return instance;
	}
	
	public void buildPool(){
		if (isPoolBuild) {
			throw new IllegalAccessError("Pool already initilzed cannot be initlized again");
		}
		
		pool = Executors.newFixedThreadPool(100);
		isPoolBuild = true;
	}

	public ExecutorService getPool() {
		return pool;
	}

	public void poolShutdown() {
		try {
			if (pool != null) {
				pool.awaitTermination(1, TimeUnit.SECONDS);
				pool.shutdown();
				if (pool.isShutdown() == false) {
					pool.shutdownNow();
				}
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
