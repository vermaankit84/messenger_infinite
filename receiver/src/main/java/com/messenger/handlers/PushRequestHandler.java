package com.messenger.handlers;

import java.util.Map;

import com.messenger.defaulthandler.Abstracthandler;

public final class PushRequestHandler extends Abstracthandler {

    private Map<String, String> mRequest = null;

    public PushRequestHandler(Map<String, String> mRequest) {
        this.mRequest = mRequest;
    }   
    @Override
	public String call() throws Exception {
		return doProcess(mRequest);
	}
}
