package com.messenger.template;

import org.apache.log4j.Logger;

public abstract class BrdTemplate implements Runnable {
	private final Logger logger = Logger.getLogger(BrdTemplate.class.getName());
	@Override
	public final void run() {
		try {
			doProcess();
		} catch (Exception e) {
			logger.warn("Exception arises while processing request " + e.getMessage(), e);
		}
	};

	public abstract void doProcess();
}
