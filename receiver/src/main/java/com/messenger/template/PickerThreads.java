package com.messenger.template;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import org.apache.log4j.Logger;

import com.messenger.constants.ReceiverConstants;
import com.messenger.db.DBConn;
import com.messenger.events.RequestEventManager;
import com.messenger.utils.ReceiverUtils;

public abstract class PickerThreads implements Runnable {
	
	private String tableName = null;
	private String dumpTableName = null;
	private final Logger logger = Logger.getLogger(PickerThreads.class.getName());
	
	private String identifier = null;
	private int rowCount = 0;
	public PickerThreads(final String tableName , final String identifier , final int rowCount) {
		this.tableName = tableName;
		this.dumpTableName = tableName + "_dump";
		this.identifier = identifier;
		this.rowCount = rowCount;
		executeTransaction();
	}
	
	public final void executeProcess() {
        try {
            final int noOfRows = markPick();
            if (noOfRows > 0) {
                executeTransaction();
            }
        } catch (Exception e) {
            logger.warn("Exception arises while migrating data from " + tableName + " to " + dumpTableName , e);
        }
    }
    public final int markPick() {
    	String query = "UPDATE "+ tableName + " set status = ? where status = ? limit " + this.rowCount;
		int noOfRowUpdated = 0;
		PreparedStatement psmt = null;
		Connection con = null;
		try {
			con = DBConn.getconnetion();
			psmt = con.prepareStatement(query);
			psmt.setInt(1, 9);
			psmt.setInt(2, 0);
			noOfRowUpdated = psmt.executeUpdate();
		} catch (Exception ex) {
			logger.warn("Error in " + this.getClass().getName() + "In method markPick " + ex.getMessage() , ex);
		} finally {
			DBConn.releaseDbConnection(null, null, con, psmt);
		}
		return noOfRowUpdated;
    }

    private void executeTransaction() {
    	Connection con = null;
		Statement stmt = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		String finishpendingquery =  "SELECT COUNT(1),MESSAGEPRIORITY,DEPARTMENTCODE FROM " + tableName + " WHERE STATUS = ? AND RETRYATTEMPT "
				+ "< 3 GROUP BY MESSAGEPRIORITY,DEPARTMENTCODE ORDER BY MESSAGEPRIORITY";
		List<String> result = null;
		try {
			con = DBConn.getconnetion();
			psmt = con.prepareStatement(finishpendingquery);
			psmt.setInt(1, 9);
			rs = psmt.executeQuery();
			result = ReceiverUtils.buildVectorFromResultSet(rs);
		} catch (Exception e) {
			logger.warn("Error in " + this.getClass().getName() + "In method finishPending " + e.getMessage() , e);
		} finally {
			DBConn.releaseDbConnection(stmt, rs, con, psmt);
		}
		if (result != null && !result.isEmpty()) {
			for (int loop = 0; loop < result.size(); loop = loop + 3) {
				String rowCount = result.get(loop + 0);
				String msgPriority = result.get(loop + 1);
				String depCode = result.get(loop + 2);
				String appRequestTime = ReceiverUtils.getCurrentDateTime();
				RequestEventManager.addRequest(depCode, Integer.parseInt(rowCount));
				shiftInBuffer(depCode, msgPriority, appRequestTime, rowCount , String.valueOf(9));
				shiftInMISTable(depCode, msgPriority, appRequestTime , String.valueOf(9));
				updateTempTable(depCode, msgPriority , String.valueOf(9));
			}
		}
    }

    private void shiftInMISTable(final String depCode,final String msgPriority,final String appRequestTime , String status) {
		Connection conn = null;
		PreparedStatement psmt = null;
		int noOfRows = 0;
		String appResponseId = ReceiverUtils.getResponseId(depCode);
		String insertMISQuery = "INSERT IGNORE INTO MIS_REC_LOG (MESSAGEID,APPRESPONSEID,DIVISIONID,DELIVERYFLAG,MSGTYPE,MSISDN,PRIORITY,MESSAGETEXT,SMSLENGTH,DNDFLAG,INTFLAG,REQDUMPDATETIME , SENDERNAME )"
				+ "SELECT CONCAT('" + identifier +  "-', MESSAGEID) , ? , DEPARTMENTCODE , ? , MESSAGETYPE , PHONENUMBER , MESSAGEPRIORITY ,MESSAGETEXT , LENGTH(MESSAGETEXT) ,DNDFLAG , INTERNATIONALFLAG , "
				+ "DATE_FORMAT(?, '%Y-%m-%d %H:%i:%s') , SENDERNAME FROM  " + tableName 
				+ " WHERE STATUS=? AND DEPARTMENTCODE = ? AND MESSAGEPRIORITY=?";
		try {
			conn = DBConn.getconnetion();
			psmt = conn.prepareStatement(insertMISQuery);
			psmt.setString(1, appResponseId);
			psmt.setString(2, "1");
			psmt.setString(3, appRequestTime);
			psmt.setString(4, status);
			psmt.setString(5, depCode);
			psmt.setString(6, msgPriority);
			noOfRows = psmt.executeUpdate();
		} catch (Exception ex) {
			logger.warn("Exception arises while migrating data from transaction table to rec table ", ex);
		} finally {
			DBConn.releaseDbConnection(null, null, conn, psmt);
		}
		if (noOfRows > 0) {
			logger.info("No of rows updated [ " + tableName + " ] data migrated to mis table  [ " + noOfRows + " ] ");
		}
	}

	

	private void shiftInBuffer(String depCode, String msgPriority, String appRequestTime,String rowCount , String status) {
		Connection connection = null;
		PreparedStatement psmt = null;
		String pBufferTableName = ReceiverUtils.getBufferTable();
		int noOfRows = 0;
		final String strQuery = "INSERT IGNORE INTO %BUFFERTABLE% (RESPONSEID,RECEIVEDSDATE,DELIVERYFLAG,CONTENTTYPE,MSISDN,MSGPRIORITY,MESSAGETEXT,DIVISIONID,INTERNATIONALFLAG,STATUS,MESSAGETYPE,SENDERNAME , DNDFLAG , VENDORORIGIN) "
				+ "SELECT CONCAT ('" + identifier +  "-' , MESSAGEID),SUBMITTEDTIME, ? ,CONTENTTYPE,PHONENUMBER,MESSAGEPRIORITY,MESSAGETEXT,DEPARTMENTCODE,INTERNATIONALFLAG,?,MESSAGETYPE , SENDERNAME  ,"
				+ "DNDFLAG , VENDORORIGIN FROM " + tableName + " WHERE STATUS = ? AND DEPARTMENTCODE= ? AND MESSAGEPRIORITY= ?";
		try {
			String insertQuery = strQuery.replaceAll("%BUFFERTABLE%", pBufferTableName);
			connection = DBConn.getconnetion();
			psmt = connection.prepareStatement(insertQuery);
			psmt.setString(1, "1");
			psmt.setString(2, ReceiverConstants.STR_MESSENGER_BUFFER_STATUS);
			psmt.setString(3, status);
			psmt.setString(4, depCode);
			psmt.setString(5, msgPriority);
			noOfRows = psmt.executeUpdate();
		} catch (Exception ex) {
			logger.warn("Exception arises while migrating data from transaction table to Queue", ex);
		} finally {
			DBConn.releaseDbConnection(null, null, connection, psmt);
		}
		if (noOfRows > 0) {
			logger.info("No of rows updated [ " + tableName + " ] data migrated to queue [ " + pBufferTableName + " ] [ " + noOfRows + " ] ");
		}
	}

	private void updateTempTable(String depCode, String msgPriority , String status) {
		Connection conn = null;
		PreparedStatement prep = null;
		int noOfRows = 0;
		final String strQuery = "INSERT IGNORE INTO "+ dumpTableName +" (MESSAGEID,DEPARTMENTCODE,SENDERNAME,PHONENUMBER,MESSAGEPRIORITY,MESSAGETEXT,SIURL, STATUS,RETRYATTEMPT,INTERNATIONALFLAG, MESSAGETYPE,SUBMITTEDTIME,CONTENTTYPE, DNDFLAG , VENDORORIGIN) "
				+ "SELECT MESSAGEID,DEPARTMENTCODE,SENDERNAME,PHONENUMBER,MESSAGEPRIORITY,MESSAGETEXT,SIURL, STATUS,RETRYATTEMPT + 1,"
				+ "INTERNATIONALFLAG, MESSAGETYPE,SUBMITTEDTIME,CONTENTTYPE, DNDFLAG , VENDORORIGIN FROM "
				+  tableName + " WHERE STATUS = ? AND DEPARTMENTCODE= ? AND MESSAGEPRIORITY=?";
		try {
			conn = DBConn.getconnetion();
			prep = conn.prepareStatement(strQuery);
			prep.setString(1, status);
			prep.setString(2, depCode);
			prep.setString(3, msgPriority);
			noOfRows = prep.executeUpdate();
		} catch (Exception ex) {
			logger.warn("Exception arises while migrating data from transaction table to dump table", ex);
		} finally {
			DBConn.releaseDbConnection(null, null, conn, prep);
		}
		if (noOfRows > 0) {
			logger.info("No of rows migrated from [ " + tableName + " ] to [ " + dumpTableName + " ] [ " + noOfRows + " ] ");
			deletFromTempTable(depCode, msgPriority , status);
		}
	}

	private void deletFromTempTable(String depCode, String msgPriority, String status) {
		Connection conn = null;
		PreparedStatement prep = null;
		int noOfRows = 0;
		final String strQuery = "DELETE FROM " + tableName + " WHERE STATUS=? AND DEPARTMENTCODE= ? AND MESSAGEPRIORITY=?";
		try {
			conn = DBConn.getconnetion();
			prep = conn.prepareStatement(strQuery);
			prep.setString(1, status);
			prep.setString(2, depCode);
			prep.setString(3, msgPriority);
			noOfRows = prep.executeUpdate();
		} catch (Exception ex) {
			logger.warn("Exception arises while deleting data from transaction table to dump table", ex);
		} finally {
			DBConn.releaseDbConnection(null, null, conn, prep);
		}
		if (noOfRows > 0) {
			logger.info("No of rows deleted [ " + tableName + " ]  [ " + noOfRows + " ] ");
		}
	}

    @Override
    public abstract void run();
    
}
