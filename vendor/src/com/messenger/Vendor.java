package com.messenger;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Vendor extends HttpServlet {
	
	private static final String CONTENT_TYPE = "text/plain";
	private static final long serialVersionUID = 1L;
	private final AtomicInteger ai = new AtomicInteger(1);
	private final transient Logger logger = Logger.getLogger(Vendor.class.getName());
	/*
	 * (non-Javadoc)
	 * @see javax.servlet.GenericServlet#destroy()
	 * As a default method hence this method is empty
	 */
	@Override
	public void init() throws ServletException {
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType(CONTENT_TYPE);
		try {
			final PrintWriter out = response.getWriter();
			final String strMsisdn = request.getParameter("msisdn");
			final String messageId = request.getParameter("msgId");
			out.write(ai.incrementAndGet() + ":" + UUID.randomUUID().toString() + ":" + strMsisdn + ":" + messageId);
		} catch (Exception e) {
			logger.warning("Execption arise" + e);
		}
		
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doGet(request, response);
		} catch (Exception e) {
			logger.warning("Execption arise" + e);
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.GenericServlet#destroy()
	 * As a default method hence this method is empty
	 */
	@Override
	public void destroy() {
	}


}
