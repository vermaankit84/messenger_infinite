package com.example;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.ibatis.jdbc.ScriptRunner;

public class SqlLoader {
	public static void main(String[] args) {
		Connection con = null;
		try {
			System.out.println("Start Execution ");
			Class.forName("com.mysql.jdbc.Driver");
			try {
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "test", "test");
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			String sqlPath = "create.sql";

			try {
				ScriptRunner sr = new ScriptRunner(con);
				Reader reader = new BufferedReader(new FileReader(sqlPath));
				sr.runScript(reader);
			} catch (Exception e) {
				System.err.println("Failed to Execute" + sqlPath + " The error is " + e.getMessage());
			}

		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("Executed Sucessfully");
	}
}
