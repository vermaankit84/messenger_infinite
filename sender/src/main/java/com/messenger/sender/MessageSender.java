package com.messenger.sender;

import java.util.Map;

import com.messenger.send.ProcessRequest;

public final class MessageSender extends SenderTemplate {
	
	public MessageSender(final Map<String, String> message , final ProcessRequest processRequest) {
		super(message , processRequest);
	}
}
