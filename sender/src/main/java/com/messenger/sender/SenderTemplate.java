package com.messenger.sender;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.messenger.constants.CoreConstants;
import com.messenger.constants.DataConstants;
import com.messenger.constants.MessageCategories;
import com.messenger.constants.VendorType;
import com.messenger.forwarder.DeleteProcessMessages;
import com.messenger.forwarder.UpdateProcessedMessages;
import com.messenger.forwarder.UpdateUnProcessedMessages;
import com.messenger.mapping.DivisonVendorMapping;
import com.messenger.repositories.VendorRepository;
import com.messenger.send.ProcessRequest;
import com.messenger.utils.Utilities;
import com.messenger.utils.ValidationUtils;
import com.messenger.valueobjects.MessageVO;

public abstract class SenderTemplate implements Sender {
	
	private Map<String, String> message = null;
	private final Logger logger = Logger.getLogger(MessageSender.class.getName());
	private ProcessRequest processRequest = null;
	public SenderTemplate(final Map<String, String> message, final ProcessRequest processRequest) {
		this.processRequest = processRequest;
		this.message = message;
	}

	@Override
	public final void run() {
		if (message != null && !message.isEmpty()) {
			try {
				doProcess();
			} catch (Exception e) {
				logger.warn("Exception arise while serving request ", e);
				UpdateUnProcessedMessages.addReOpenMessage(message.get(DataConstants.STR_RESPONSE_ID), message.get(DataConstants.STR_MSISDN));
			}
		}
	}

	@Override
	public void doProcess() throws Exception {
		final String mResponseId = message.get(DataConstants.STR_RESPONSE_ID);
		final String mMobileNumber = message.get(DataConstants.STR_MSISDN);
		final String mMessage = Utilities.decode(Utilities.encode(message.get(DataConstants.STR_MESSAGE_TEXT).trim()));
		final String mMessageType =  message.get(DataConstants.STR_MESSAGE_TYPE).trim();
		final String mSender = message.get(DataConstants.STR_SENDER_NAME).trim();
		final String mIntFlag = message.get(DataConstants.STR_INT_FLAG) != null && !message.get(DataConstants.STR_INT_FLAG).isEmpty()? message.get(DataConstants.STR_INT_FLAG).trim() : "0";
		final String mDivisionId = message.get(DataConstants.STR_DIVISION_ID).trim();
		final String dumpDate = message.get(DataConstants.STR_RECEIVED_DATE).trim();
		final String contentType = message.get(DataConstants.STR_CONTENTTYPE) != null && !message.get(DataConstants.STR_CONTENTTYPE).isEmpty()? message.get(DataConstants.STR_CONTENTTYPE).trim() : "0";
		final String vendorOrigin = message.get(DataConstants.STR_VENDOR_ORGIN)!=null && !message.get(DataConstants.STR_VENDOR_ORGIN).trim().isEmpty() ? message.get(DataConstants.STR_VENDOR_ORGIN) : null;
		final int messType = message.get(DataConstants.STR_DND_FLAG)!=null && !message.get(DataConstants.STR_DND_FLAG).trim().isEmpty()? Integer.valueOf(message.get(DataConstants.STR_DND_FLAG).trim()) : 3;
		final String validationString = ValidationUtils.validateRequest(mMessageType, mDivisionId, mMessage,mMobileNumber, mResponseId, dumpDate, mSender);
		if (validationString != null && !validationString.isEmpty()) {
			logger.info("message id [ " + mResponseId + " ] rejected with [ " + validationString+ " ] having msisdn [ " + mMobileNumber + "  ] ");
			UpdateProcessedMessages.addCloseMessage("","R", validationString, "",mMobileNumber, mResponseId, "", dumpDate);
			DeleteProcessMessages.addDeleteMessage(mResponseId, mMobileNumber);
			return;
		}
		processMessage(new MessageVO(mResponseId, mMobileNumber, mMessage, mMessageType, mSender, mIntFlag, mDivisionId, contentType, MessageCategories.getMessageCategory(messType) , vendorOrigin));
	}

	private void processMessage(final MessageVO messageVO) throws Exception {
		final List<Integer> vendorOrginList = getVendorOrginList(messageVO.getVendorOrigin() , messageVO.getMessageCategories());
		String strResponse = null;
		if (vendorOrginList!=null && !vendorOrginList.isEmpty()) {
			strResponse = sendMessage(vendorOrginList, messageVO);
		}
		List<Integer> prefVendorList = null;
		if (strResponse == null) {
			prefVendorList =  getPrefvendorList(messageVO.getmDivisionId(), messageVO.getmMessageType());		
			if (prefVendorList != null && !prefVendorList.isEmpty()) {
				strResponse = sendMessage(prefVendorList, messageVO);
			}
		}
		if (strResponse == null) {
			strResponse = sendDefaultVendors(messageVO , prefVendorList);
		}

		if (strResponse == null) {
			UpdateUnProcessedMessages.addReOpenMessage(messageVO.getmResponseId(), messageVO.getmMobileNumber());
		}
	}

	private List<Integer> getVendorOrginList(final String vendorOrigin, final MessageCategories messageCategories) {
		if (vendorOrigin == null || vendorOrigin.isEmpty()) {
			return null;
		}
		
		List<Integer> vendorList = null;
		switch (messageCategories) {
			case OTP_MESSAGE:
				vendorList = VendorRepository.getInstance().getVendorList(v -> (VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_OTP ||VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_EMAIL_OTP) && (vendorOrigin.equalsIgnoreCase(VendorRepository.getInstance().getVendorOrign(v))));
				break;
			case PROMO_MESSAGE:
				vendorList = VendorRepository.getInstance().getVendorList(v -> (VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_PROMO ||VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_EMAIL_PROMO) && (vendorOrigin.equalsIgnoreCase(VendorRepository.getInstance().getVendorOrign(v))));
				break;
			case TRANS_MESSAGE:
				vendorList = VendorRepository.getInstance().getVendorList(v -> (VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_TRANS ||VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_EMAIL_TRANS) && (vendorOrigin.equalsIgnoreCase(VendorRepository.getInstance().getVendorOrign(v))));
			break;
		}
		return vendorList;
	}

	private String sendDefaultVendors(final MessageVO messageVO , final List<Integer> prefVendorList) throws Exception {
		String strResponse = null;
		MessageCategories messageCategories = null;
		List<Integer> vendorList = null;
		switch (messageVO.getmMessageType().toUpperCase()) {
		case "S":
			messageCategories = messageVO.getMessageCategories();
			switch (messageCategories) {
			case OTP_MESSAGE:
				vendorList = VendorRepository.getInstance().getVendorList(v ->(VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_OTP) || (VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_EMAIL_OTP));
				strResponse = sendMessage(vendorList, messageVO );
				if (strResponse!=null && !strResponse.isEmpty()) {
					break;
				}
				
			case TRANS_MESSAGE:
				vendorList = VendorRepository.getInstance().getVendorList(v ->(VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_TRANS) || (VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_EMAIL_TRANS));
				if (strResponse!=null && !strResponse.isEmpty()) {
					break;
				}
			case PROMO_MESSAGE:
				vendorList = VendorRepository.getInstance().getVendorList(v ->(VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_PROMO) || (VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_EMAIL_PROMO));
				strResponse = sendMessage(vendorList, messageVO);
				if (strResponse!=null && !strResponse.isEmpty()) {
					break;
				}
				break;
			}
			break;
		case "E":
			messageCategories = messageVO.getMessageCategories();
			switch (messageCategories) {				
			case OTP_MESSAGE:
				vendorList = VendorRepository.getInstance().getVendorList(v ->(VendorRepository.getInstance().getVendorType(v) == VendorType.EMAIL_OTP) || (VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_EMAIL_OTP) , prefVendorList);
				strResponse = sendMessage(vendorList, messageVO);
				if (strResponse!=null && !strResponse.isEmpty()) {
					break;
				}
			case TRANS_MESSAGE:
				vendorList = VendorRepository.getInstance().getVendorList(v ->(VendorRepository.getInstance().getVendorType(v) == VendorType.EMAIL_TRANS) || (VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_EMAIL_TRANS) , prefVendorList);
				strResponse = sendMessage(vendorList, messageVO);
				if (strResponse!=null && !strResponse.isEmpty()) {
					break;
				}
			case PROMO_MESSAGE:
				vendorList = VendorRepository.getInstance().getVendorList(v ->(VendorRepository.getInstance().getVendorType(v) == VendorType.EMAIL_PROMO) || (VendorRepository.getInstance().getVendorType(v) == VendorType.SMS_EMAIL_PROMO) , prefVendorList);
				strResponse = sendMessage(vendorList, messageVO);
				if (strResponse!=null && !strResponse.isEmpty()) {
					break;
				}
				break;
			}
			break;
		default:
			throw new UnsupportedOperationException("This message type is not supported [ " + messageVO.getmMessageType() + " ] ");
		}
		return strResponse;
	}

	private String sendMessage(final List<Integer> vendorList , final MessageVO messageVO) throws Exception {
		logger.info("vendor list obtained for Message type [ " + messageVO.getmMessageType() + " ]  vendor List obtained [ " + vendorList + " ] message id [ "+ messageVO.getmResponseId() + " ] ");
		if (vendorList == null || vendorList.isEmpty()) {
			return null;
		}
		String strResponse = null;
		final List<Integer> sortedVendorList = VendorRepository.getInstance().sortByPriority(vendorList);
		if (sortedVendorList!=null && !sortedVendorList.isEmpty()) {
			for (int vendorId : sortedVendorList) {
				logger.info("Trying to send message [ " + messageVO.getmResponseId() + " ]  via vendor [ " + vendorId + " ] msisdn [ "+ messageVO.getmMobileNumber() + " ] ");
				final String[] venderResponse =  processRequest.sendMessage(vendorId, messageVO.getmMessage(), messageVO.getmMobileNumber(), messageVO.getmSender(), messageVO.getmResponseId(), messageVO.getmIntFlag(), messageVO.getmMessageType(), messageVO.getContentType());
				if (venderResponse!=null && venderResponse.length == 3) {
					processVendorResponse(venderResponse, messageVO.getmResponseId(), messageVO.getmMobileNumber());
					strResponse = venderResponse[1].trim();
					break;
				}
			}
		}			
		return strResponse;
	}

	private List<Integer> getPrefvendorList(String divisionId, String messageType) {
		return DivisonVendorMapping.getInstance().getVendorList(divisionId , messageType);		
	}
	
	private final void processVendorResponse(String[] venderResponse, String mResponseId, String mMobileNumber) {
		logger.info("vendor response id obtained [ " + venderResponse[1].trim() + " ] for vendor [ "+ VendorRepository.getInstance().getVendorName(Integer.parseInt(venderResponse[0].trim())) + " ]");
		UpdateProcessedMessages.addCloseMessage(venderResponse[0], CoreConstants.PROCESS_SUBMIT,CoreConstants.PROCESS_SUBMIT_DESC, venderResponse[1].trim(), mMobileNumber, mResponseId,venderResponse[2], Utilities.getCurrentDateTime());
		DeleteProcessMessages.addDeleteMessage(mResponseId, mMobileNumber);
	}	
}
