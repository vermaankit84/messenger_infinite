package com.messenger.sender;

public interface Sender extends Runnable {
	public void doProcess() throws Exception;
}
