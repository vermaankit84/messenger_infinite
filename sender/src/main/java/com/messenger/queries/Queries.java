package com.messenger.queries;

import com.messenger.constants.CoreConstants;
import com.messenger.properties.CoreProperties;

public interface Queries {

	String STR_SUB_UPDATE_QUERY = CoreProperties.getInstance().getValue("STR_SUB_UPDATE_QUERY").replaceAll("%SENDER_TABLE_NAME", CoreConstants.SENDER_TABLE_NAME);
	String STR_FAILURE_EVT_MGR = CoreProperties.getInstance().getValue("STR_FAILURE_EVT_MGR");
	String STR_SUCESS_EVT_MGR = CoreProperties.getInstance().getValue("STR_SUCESS_EVT_MGR");
	String STR_SET_DOWN_TIME = CoreProperties.getInstance().getValue("STR_SET_DOWN_TIME");
	String STR_SET_UP_TIME = CoreProperties.getInstance().getValue("STR_SET_UP_TIME");
	String STR_UPDATE_EVT_MGR = CoreProperties.getInstance().getValue("STR_UPDATE_EVT_MGR");
	String STR_DELETE_PROCESS_MESSAGES = CoreProperties.getInstance().getValue("STR_DELETE_PROCESS_MESSAGES").replaceAll("%SENDER_TABLE_NAME", CoreConstants.SENDER_TABLE_NAME);
	String STR_INSERT_INTO_MIS = CoreProperties.getInstance().getValue("STR_INSERT_INTO_MIS");
	String STR_UPDATE_UNP_MESS = CoreProperties.getInstance().getValue("STR_UPDATE_UNP_MESS").replaceAll("%SENDER_TABLE_NAME", CoreConstants.SENDER_TABLE_NAME);
	String STR_SELECT_DIV = CoreProperties.getInstance().getValue("STR_SELECT_DIV");
	String STR_DIV_VENDOR_MAPPING = CoreProperties.getInstance().getValue("STR_DIV_VENDOR_MAPPING");
	String STR_VENDOR_REP_QUERY = CoreProperties.getInstance().getValue("STR_VENDOR_REP_QUERY");
	String STR_MESS_VENDORMASTER = CoreProperties.getInstance().getValue("STR_MESS_VENDORMASTER");
	String STR_MESS_TEMPVENDOR = CoreProperties.getInstance().getValue("STR_MESS_TEMPVENDOR");
	String STR_UPDATE_VENDOR_TIME = CoreProperties.getInstance().getValue("STR_UPDATE_VENDOR_TIME");
	
}
