package com.messenger.repositories;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

import com.messenger.conn.DBSubmitConn;
import com.messenger.utils.Utilities;

public class SenderRepository {

	private static Map<String, List<String>> mRepository = new HashMap<>();
	private static SenderRepository mInstance = null;
	private static final Logger log = Logger.getLogger(SenderRepository.class.getName());
	private SenderRepository() {
	}

	public static synchronized  SenderRepository getInstance() {
		if (mInstance == null) {
			mInstance = new SenderRepository();
			loadRepository();
		}
		return mInstance;
	}

	private static synchronized void loadRepository() {

		String strQuery = "select md.DIVISIONID , msm.SENDER_NAME  "
				+ "from mess_division md inner join mess_sender_master msm on msm.DIVISIONID = md.DIVISIONID and msm.SENDER_IS_ACTIVE = 1";
		Connection conn = null;
		Statement stat = null;
		ResultSet rs = null;
		List<String> result = null;
		
		try {
			conn = DBSubmitConn.getconnetion();
			stat = conn.createStatement();
			rs = stat.executeQuery(strQuery);
			result = Utilities.buildVectorFromResultSet(rs);
		} catch (Exception ex) {
			log.warn(" ****************** EXCEPTION ARISES IN SENDER REPOSITORY ****************** " , ex);
		} finally {
			DBSubmitConn.releaseDbConnection(stat, rs, conn, null);
		}

		if (result != null && !result.isEmpty()) {		
			mRepository.clear();
			for (int i = 0; i < result.size(); i = i + 2) {
				String divisonID = result.get(i + 0);
				String sendername = result.get(i + 1);
				List<String> senderList = null;
				if(mRepository.containsKey(divisonID)){
					senderList = mRepository.get(divisonID);
					senderList.add(sendername);
					mRepository.put(divisonID, senderList);
				} else {
					senderList =  new ArrayList<>();
					senderList.add(sendername);
					mRepository.put(divisonID, senderList);
				}
			}
		}
	}

	public boolean isValidSender(String divID , String sendername) {
		boolean isValidSender = false;		
		if (mRepository != null && !mRepository.isEmpty()) {
			List<String> senderList = mRepository.get(divID);
			isValidSender = senderList.parallelStream().anyMatch(s -> s.equalsIgnoreCase(sendername));
		}		
		return isValidSender;
	}

	public void reloadRepository() {
		loadRepository();
	}

}
