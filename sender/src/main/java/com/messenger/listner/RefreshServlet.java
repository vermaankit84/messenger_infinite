package com.messenger.listner;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.messenger.mapping.DivisonVendorMapping;
import com.messenger.repositories.SenderRepository;
import com.messenger.repositories.TemplateRepository;
import com.messenger.repositories.VendorRepository;

public class RefreshServlet extends HttpServlet {

	private static final String CONTENT_TYPE = "text/plain";
	private static final long serialVersionUID = 1L;

	@Override
	public void init() throws ServletException {

	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		response.setContentType(CONTENT_TYPE);
		final PrintWriter out = response.getWriter();
		String responsestring = "NO ACTION FOUND";
		String action = request.getParameter("action");
		if (action != null && action.equalsIgnoreCase("refresh")) {
			String mode = request.getParameter("mode");
				switch (mode) {
				case "vendor":
					VendorRepository.getInstance().reloadRepository();
					responsestring = "Vendor repository reloaded successfully";
					break;
				case "divisionMapping" :
					DivisonVendorMapping.getInstance().reloadRepository();
					responsestring = "Division vendor mapping updated successfully";
					break;
				case "template":
					TemplateRepository.getInstance().reloadRepository();
					responsestring = "Template mapping has been updated successfully";
					break;
				case "sender":
					SenderRepository.getInstance().reloadRepository();
					responsestring = "sender repository sucessfully refreshed";
					break;		
				default:
					break;
				}
		}
		out.write(responsestring);
	}
	
	@Override
	public void destroy() {
		
	}
}
