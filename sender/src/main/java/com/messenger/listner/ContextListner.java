package com.messenger.listner;

import java.io.IOException;
import java.lang.management.ManagementFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;

import com.messenger.conn.DBSubmitConn;
import com.messenger.constants.CoreConstants;
import com.messenger.httppool.HttpClientManager;
import com.messenger.server.SenderServer;
import com.messenger.utils.Utilities;

public class ContextListner implements ServletContextListener {
	
	private final Logger logger = Logger.getLogger(ContextListner.class.getName());

	@Override
	public void contextDestroyed(final ServletContextEvent sce) {
		try {
			SenderServer.getInstance().stopSubmitter();
		} catch (InterruptedException e) {
			logger.warn("Exception arises while shutting down application " ,  e);
		}
		try {
			HttpClientManager.getInstance().closepoolconnection();
		} catch (IOException e) {
			
		}
		DBSubmitConn.closeDataSource();
	}

	@Override
	public void contextInitialized(final ServletContextEvent sce) {
		createdbpool();
		HttpClientManager.getInstance().buildClientManager();
		logger.info("Sender server has been start at [ " + Utilities.getCurrentDateTime() + " ] process id [ " + ManagementFactory.getRuntimeMXBean().getName().split("@")[0] + " ] on queue [ " +  CoreConstants.SENDER_TABLE_NAME + " ] ");
		SenderServer.getInstance().startSubmitter();
	}

	private void createdbpool() {
		try {
			Context iniCon = new InitialContext();
			Context ctx = (Context) iniCon.lookup("java:/comp/env");
			logger.info("messenger connection ::::get initial context" + new StringBuilder().append("initial context is ").append(String.valueOf(ctx)).toString());
			if (ctx != null) {
				DataSource	dataSource = (DataSource) ctx.lookup("sender");
				DBSubmitConn.setDataSource(dataSource);
				logger.info("messenger db connection ::::getDataSource" +  new StringBuilder().append("datasource is ").append(String.valueOf(dataSource)).toString());
			}			
		} catch (Exception e) {
			logger.warn("exception arises while loading db connection " + e.getMessage() , e);
		}
	}
}
