package com.messenger.send;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.messenger.events.SuccessEventManager;
import com.messenger.httppool.HttpClientManager;
import com.messenger.properties.CoreProperties;
import com.messenger.repositories.VendorRepository;
import com.messenger.response.ParseVendorResponse;
import com.messenger.sender.MessageSender;
import com.messenger.utils.Utilities;

public class ProcessRequest {
	
	private final Logger logger = Logger.getLogger(MessageSender.class.getName());
	private final int maxRetry = Integer.parseInt(CoreProperties.getInstance().getValue("MAX_COUNT" , "2"));
	private final ParseVendorResponse parseRespone = new ParseVendorResponse();
	
	public final String[] sendMessage (int vendorId, String mMessage, String mMobileNumber, String mSenderId, String mResponseId, String mInternationalFlag, String mMessageType , String contenttype) throws Exception {		
		final String pushVendorURL  = VendorRepository.getInstance().getVendorUrl(vendorId);            
		for (int i = 1; i < maxRetry ; i = i + 1) {
			final String venderResponse  = sendPlainText(mMessage, pushVendorURL, vendorId, mMobileNumber, mSenderId, mResponseId,mInternationalFlag, mMessageType, contenttype);
			if (venderResponse != null && !venderResponse.isEmpty()) {
				SuccessEventManager.addRequest(String.valueOf(vendorId), 1, 0);
				final String getCurrentDate = Utilities.getCurrentDateTime();
				logger.info("Messenger message with Id  " + mResponseId + " has been successfully submitted  " + VendorRepository.getInstance().getVendorName(vendorId) + " at " + getCurrentDate);
				return new String[] { String.valueOf(vendorId), venderResponse, getCurrentDate };
			} else {
				TimeUnit.MILLISECONDS.sleep(50);
				logger.info("Messenger message submitter  " + mResponseId + " Retry" + i + "at [ " + Utilities.getCurrentDateTime() + " ] ");
			}
		}        
        return null;
	}
	
	public final String sendPlainText(String msgtext, String pushurl, int pVendorId, String mMobileNumber, String mSenderId, String mResponseId, String mInternationalFlag, String mMessageType , String contenttype) throws IOException {  	
		String strUrl = null;
		try {
			strUrl = pushurl.replaceAll("%TEXT", Utilities.decode(msgtext)) .
					replaceAll("%TO",Utilities.decode(Utilities.encode(mMobileNumber.trim()))).
					replaceAll("%FROM",Utilities.encode(mSenderId)).replaceAll("%MSGID", Utilities.encode(mResponseId)).
					replaceAll("%INTFLAG", Utilities.encode(mInternationalFlag)).
					replaceAll("%CONTENTYPE%", Utilities.encode(contenttype)).
					replaceAll("%MSGTYPE", Utilities.encode(mMessageType));                       
		} catch (Exception ex) {
           logger.warn((" EXCEPTION ARISES WHILE PARSING PUSH URL **** HAVING TEXT" .concat(msgtext).concat("HAVING MOBILE NUMBER") .concat(mMobileNumber).concat("HAVING MOBILE NUMBER").concat(mMobileNumber) .concat("HAVING SENDER") .concat(mSenderId).concat("BANK").concat("HAVING MESSAGE ID") .concat(mResponseId).concat("HAVING INTERNATIONAL FLAG NUMBER").concat(mInternationalFlag).concat("HAVING MESSAGE TYPE").concat(mMessageType)).toLowerCase() , ex);
        }         
		String urlresponse = processRequest(strUrl,pVendorId);
        logger.info( ("MESSAGE SENDER ----> " + VendorRepository.getInstance().getVendorName(pVendorId) + "::::::" +  strUrl + "::RESPONSE RECEIVED::" +  (urlresponse!=null && !urlresponse.isEmpty() ? urlresponse :  "FAILED").toLowerCase()));            
        return urlresponse;      
	}

	

	private String processRequest(final String pushurl , final int vendorId) throws IOException {
		String urlResponse = null;		
		final String mode = VendorRepository.getInstance().getVendoSupport(vendorId);
		if ("GET".equalsIgnoreCase(mode)) {
			urlResponse = HttpClientManager.getInstance().generateGetResponse(pushurl);
		} else {
			final String url = pushurl.split("@@@")[0];
			final String urlvalues = pushurl.split("@@@")[1];
			logger.info("Sending post request via URL [ " + url + " ] values [ " + urlvalues + " ] at [ " + Utilities.getCurrentDateTime() + " ] ");
			urlResponse = HttpClientManager.getInstance().generatePostResponse(url,VendorRepository.getInstance().getHeaderDetails(vendorId),Utilities.getNameValuePair(urlvalues.split("\\&")) ,VendorRepository.getInstance().getCredentails(vendorId));
		}
		
		if (urlResponse!=null && !urlResponse.isEmpty()) {
			return parseRespone.ParseResponseId(urlResponse , vendorId);
		}
		return null;
	}
}
