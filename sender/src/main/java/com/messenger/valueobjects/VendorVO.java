package com.messenger.valueobjects;

import com.messenger.constants.VendorType;;

public final class VendorVO {

	private int vendorId = 0;
	private String vendorname = null;
	private String vendororigin = null;
	private VendorType vendortype = null;

	private String vendorurl = null;
	private int vendorpriority = 0;
	private String vendorsupport = null;
	private String vendorHeader = null;
	private String vendorCred = null;
	
	public VendorVO(int vendorId, String vendorname, String vendororigin, VendorType vendortype, String vendorurl,int vendorpriority, String vendorsupport, String vendorHeader, String vendorCred) {
		this.vendorId = vendorId;
		this.vendorname = vendorname;
		this.vendororigin = vendororigin;
		this.vendortype = vendortype;
		this.vendorurl = vendorurl;
		this.vendorpriority = vendorpriority;
		this.vendorsupport = vendorsupport;
		this.vendorHeader = vendorHeader;
		this.vendorCred = vendorCred;
	}
	
	public String getVendorHeader() {
		return vendorHeader;
	}

	public String getVendorCred() {
		return vendorCred;
	}
	
	public int getVendorId() {
		return vendorId;
	}
	public String getVendorname() {
		return vendorname;
	}
	public String getVendororigin() {
		return vendororigin;
	}
	public VendorType getVendortype() {
		return vendortype;
	}
	public String getVendorurl() {
		return vendorurl;
	}
	public int getVendorpriority() {
		return vendorpriority;
	}
	public String getVendorsupport() {
		return vendorsupport;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vendorCred == null) ? 0 : vendorCred.hashCode());
		result = prime * result + ((vendorHeader == null) ? 0 : vendorHeader.hashCode());
		result = prime * result + vendorId;
		result = prime * result + ((vendorname == null) ? 0 : vendorname.hashCode());
		result = prime * result + ((vendororigin == null) ? 0 : vendororigin.hashCode());
		result = prime * result + vendorpriority;
		result = prime * result + ((vendorsupport == null) ? 0 : vendorsupport.hashCode());
		result = prime * result + ((vendortype == null) ? 0 : vendortype.hashCode());
		result = prime * result + ((vendorurl == null) ? 0 : vendorurl.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendorVO other = (VendorVO) obj;
		if (vendorCred == null) {
			if (other.vendorCred != null)
				return false;
		} else if (!vendorCred.equals(other.vendorCred))
			return false;
		if (vendorHeader == null) {
			if (other.vendorHeader != null)
				return false;
		} else if (!vendorHeader.equals(other.vendorHeader))
			return false;
		if (vendorId != other.vendorId)
			return false;
		if (vendorname == null) {
			if (other.vendorname != null)
				return false;
		} else if (!vendorname.equals(other.vendorname))
			return false;
		if (vendororigin == null) {
			if (other.vendororigin != null)
				return false;
		} else if (!vendororigin.equals(other.vendororigin))
			return false;
		if (vendorpriority != other.vendorpriority)
			return false;
		if (vendorsupport == null) {
			if (other.vendorsupport != null)
				return false;
		} else if (!vendorsupport.equals(other.vendorsupport))
			return false;
		if (vendortype != other.vendortype)
			return false;
		if (vendorurl == null) {
			if (other.vendorurl != null)
				return false;
		} else if (!vendorurl.equals(other.vendorurl))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VendorVO [vendorId=" + vendorId + ", vendorname=" + vendorname + ", vendororigin=" + vendororigin + ", vendortype=" + vendortype + ", vendorurl=" + vendorurl + ", vendorpriority=" + vendorpriority + ", vendorsupport=" + vendorsupport + ", vendorHeader=" + vendorHeader + ", vendorCred=" + vendorCred+ "]";
	}	
}
