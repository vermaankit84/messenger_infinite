package com.messenger.valueobjects;

public final class TemplateVO {

	private String templateId = null;
	private String templateName = null;
	private String templateMessage = null;

	public TemplateVO(String templateId,String templateName, String templateMessage) {
		this.templateId = templateId;
		this.templateName = templateName;
		this.templateMessage = templateMessage;
	}

	public String getTemplateId() {
		return templateId;
	}

	public String getTemplateName() {
		return templateName;
	}

	public String getTemplateMessage() {
		return templateMessage;
	}

	@Override
	public String toString() {
		return "TemplateVO [templateId=" + templateId + ", templateName=" + templateName + ", templateMessage="+ templateMessage + "]";
	}
	
	
}
