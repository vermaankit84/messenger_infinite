package com.messenger.valueobjects;

import com.messenger.constants.MessageCategories;

public class MessageVO {
	private String mResponseId = null;
	private String mMobileNumber = null;
	private String mMessage = null;
	private String mMessageType = null;
	private String mSender = null;
	private String mIntFlag = null;
	private String mDivisionId = null;
	private String contentType = null;
	private MessageCategories messageCategories = null;
	private String vendorOrigin = null;

	public MessageVO(String mResponseId, String mMobileNumber, String mMessage, String mMessageType, String mSender,String mIntFlag, String mDivisionId, String contentType, final MessageCategories messageCategories , final String vendorOrigin) {
		this.mResponseId = mResponseId;
		this.mMobileNumber = mMobileNumber;
		this.mMessage = mMessage;
		this.mMessageType = mMessageType;
		this.mSender = mSender;
		this.mIntFlag = mIntFlag;
		this.mDivisionId = mDivisionId;
		this.contentType = contentType;
		this.messageCategories = messageCategories;
		this.vendorOrigin = vendorOrigin;
	}

	public MessageCategories getMessageCategories() {
		return messageCategories;
	}

	public String getmResponseId() {
		return mResponseId;
	}

	public String getmMobileNumber() {
		return mMobileNumber;
	}

	public String getmMessage() {
		return mMessage;
	}

	public String getmMessageType() {
		return mMessageType;
	}

	public String getmSender() {
		return mSender;
	}

	public String getmIntFlag() {
		return mIntFlag;
	}

	public String getmDivisionId() {
		return mDivisionId;
	}

	public String getContentType() {
		return contentType;
	}

	public String getVendorOrigin() {
		return vendorOrigin;
	}

	@Override
	public String toString() {
		return "MessageVO [mResponseId=" + mResponseId + ", mMobileNumber=" + mMobileNumber + ", mMessage=" + mMessage
				+ ", mMessageType=" + mMessageType + ", mSender=" + mSender + ", mIntFlag=" + mIntFlag
				+ ", mDivisionId=" + mDivisionId + ", contentType=" + contentType + ", messageCategories="
				+ messageCategories + ", vendorOrigin=" + vendorOrigin + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contentType == null) ? 0 : contentType.hashCode());
		result = prime * result + ((mDivisionId == null) ? 0 : mDivisionId.hashCode());
		result = prime * result + ((mIntFlag == null) ? 0 : mIntFlag.hashCode());
		result = prime * result + ((mMessage == null) ? 0 : mMessage.hashCode());
		result = prime * result + ((mMessageType == null) ? 0 : mMessageType.hashCode());
		result = prime * result + ((mMobileNumber == null) ? 0 : mMobileNumber.hashCode());
		result = prime * result + ((mResponseId == null) ? 0 : mResponseId.hashCode());
		result = prime * result + ((mSender == null) ? 0 : mSender.hashCode());
		result = prime * result + ((messageCategories == null) ? 0 : messageCategories.hashCode());
		result = prime * result + ((vendorOrigin == null) ? 0 : vendorOrigin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageVO other = (MessageVO) obj;
		if (contentType == null) {
			if (other.contentType != null)
				return false;
		} else if (!contentType.equals(other.contentType))
			return false;
		if (mDivisionId == null) {
			if (other.mDivisionId != null)
				return false;
		} else if (!mDivisionId.equals(other.mDivisionId))
			return false;
		if (mIntFlag == null) {
			if (other.mIntFlag != null)
				return false;
		} else if (!mIntFlag.equals(other.mIntFlag))
			return false;
		if (mMessage == null) {
			if (other.mMessage != null)
				return false;
		} else if (!mMessage.equals(other.mMessage))
			return false;
		if (mMessageType == null) {
			if (other.mMessageType != null)
				return false;
		} else if (!mMessageType.equals(other.mMessageType))
			return false;
		if (mMobileNumber == null) {
			if (other.mMobileNumber != null)
				return false;
		} else if (!mMobileNumber.equals(other.mMobileNumber))
			return false;
		if (mResponseId == null) {
			if (other.mResponseId != null)
				return false;
		} else if (!mResponseId.equals(other.mResponseId))
			return false;
		if (mSender == null) {
			if (other.mSender != null)
				return false;
		} else if (!mSender.equals(other.mSender))
			return false;
		if (messageCategories != other.messageCategories)
			return false;
		if (vendorOrigin == null) {
			if (other.vendorOrigin != null)
				return false;
		} else if (!vendorOrigin.equals(other.vendorOrigin))
			return false;
		return true;
	}
	
}
