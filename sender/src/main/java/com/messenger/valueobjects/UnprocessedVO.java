package com.messenger.valueobjects;

import java.io.Serializable;

public final class UnprocessedVO implements Serializable{

	private static final long serialVersionUID = 1L;
	private String pResponseId = null;
	private String pMsisdn = null;
	
	
	public UnprocessedVO(String pResponseId, String pMsisdn) {
		this.pResponseId = pResponseId;
		this.pMsisdn = pMsisdn;
	}
	
	public String getpResponseId() {
		return pResponseId;
	}
	public String getpMsisdn() {
		return pMsisdn;
	}
	
	

}
