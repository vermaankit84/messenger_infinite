package com.messenger.valueobjects;

public class SuccessEventVO {
	
	private String mVendorId;
	private int mPrimaryCount;
	private int mSecondaryCount;

	public SuccessEventVO(String pVendorId, int pPrimaryCount, int pSecondaryCount) {
		this.mVendorId = pVendorId;
		this.mPrimaryCount = pPrimaryCount;
		this.mSecondaryCount = pSecondaryCount;
	}

	public String getVendorId() {
		return mVendorId;
	}

	public int getPrimaryCount() {
		return mPrimaryCount;
	}

	public int getTotalCounts() {
		return this.mPrimaryCount + this.mSecondaryCount;
	}

	public int getSecondaryCount() {
		return mSecondaryCount;
	}

	public void setPrimaryCount(int pPrimaryCount) {
		this.mPrimaryCount = pPrimaryCount;
	}

	public void setSecondaryCount(int pSecondaryCount) {
		this.mSecondaryCount = pSecondaryCount;
	}

}
