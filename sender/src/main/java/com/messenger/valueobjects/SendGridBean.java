package com.messenger.valueobjects;

/**
 * Created by ankverma on 11/15/2016.
 */
public class SendGridBean {
    private String message = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

	@Override
	public String toString() {
		return "SendGridBean [message=" + message + "]";
	}
    
}
