package com.messenger.valueobjects;

import java.io.Serializable;

public final class DeleteVO implements Serializable{

	
	private static final long serialVersionUID = 1L;
	private String pResponseId = null;
	private String pMsisdn = null;
	
	
	public DeleteVO(String pResponseId, String pMsisdn) {
		this.pResponseId = pResponseId;
		this.pMsisdn = pMsisdn;
	}
	
	public String getpResponseId() {
		return pResponseId;
	}
	public String getpMsisdn() {
		return pMsisdn;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pMsisdn == null) ? 0 : pMsisdn.hashCode());
		result = prime * result + ((pResponseId == null) ? 0 : pResponseId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeleteVO other = (DeleteVO) obj;
		if (pMsisdn == null) {
			if (other.pMsisdn != null)
				return false;
		} else if (!pMsisdn.equals(other.pMsisdn))
			return false;
		if (pResponseId == null) {
			if (other.pResponseId != null)
				return false;
		} else if (!pResponseId.equals(other.pResponseId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DeleteVO [pResponseId=" + pResponseId + ", pMsisdn=" + pMsisdn + "]";
	}

}
