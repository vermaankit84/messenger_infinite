package com.messenger.router;

import java.util.Map;

import com.messenger.send.ProcessRequest;

public interface TaskFactory {
	Runnable createTask(final Map<String, String> message , final ProcessRequest processRequest);
}
