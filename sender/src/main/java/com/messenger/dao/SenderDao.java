package com.messenger.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.messenger.conn.DBSubmitConn;
import com.messenger.constants.CoreConstants;
import com.messenger.constants.DataConstants;
import com.messenger.forwarder.UpdateUnProcessedMessages;
import com.messenger.properties.CoreProperties;
import com.messenger.queries.Queries;


public final class SenderDao {

    private final Logger logger = Logger.getLogger(SenderDao.class.getName());
	public void updateRejectedMessages(final List<Map<String, String>> messages) {
		if (messages != null && !messages.isEmpty()) {
			for (int i = 0; i < messages.size(); i++) {
				final Map<String, String> messageMap = messages.get(i);
				UpdateUnProcessedMessages.addReOpenMessage(messageMap.get(DataConstants.STR_RESPONSE_ID), messageMap.get(DataConstants.STR_MSISDN));
			}
		}
	}
    
    public  boolean doUpdateResponseId(final List<Map<String, String>> messages){
        Connection con = null;
        PreparedStatement psmt = null;

        int[] noOfRecordUpdated = null;
        if (!messages.isEmpty()) {
            try {
                con = DBSubmitConn.getconnetion();
                psmt = con.prepareStatement(Queries.STR_SUB_UPDATE_QUERY);

                for (int loop = 0; loop < messages.size(); loop = loop + 1) {
                    final Map<String, String> message = messages.get(loop);
                    psmt.setString(1, CoreConstants.MESSAGE_STATUS_PROGRESS);
                    psmt.setString(2, message.get(DataConstants.STR_RESPONSE_ID));
                    psmt.setString(3, message.get(DataConstants.STR_MSISDN));
                    psmt.addBatch();
                }
                noOfRecordUpdated = psmt.executeBatch();
            } catch (Exception e) {
            	logger.warn("Exception arises while updating data ", e);
            } finally {
            	DBSubmitConn.releaseDbConnection(null, null, con, psmt);
            }
        }
		
        return noOfRecordUpdated!=null && noOfRecordUpdated.length > 0;
    }

    public  List<Map<String, String>> getBufferedMessages() throws Exception {
        List<Map<String, String>> resultSetVector = new ArrayList<>();
        Connection conn = null;
        PreparedStatement prep = null;
        ResultSet resultSet = null;

        final String query = CoreProperties.getInstance().getValue(CoreConstants.SENDER_TABLE_NAME + "_SELECT_QUERY".toLowerCase());
        try {
            conn = DBSubmitConn.getconnetion();
            prep = conn.prepareStatement(query);
            prep.setInt(1, 31);
            resultSet = prep.executeQuery();
            ResultSetMetaData rsmDataObject = resultSet.getMetaData();
            while (resultSet.next()) {
                final Map<String, String> message = new HashMap<>();
                for (int i = 0; i < rsmDataObject.getColumnCount(); i++) {
                    message.put(rsmDataObject.getColumnName(i + 1).toUpperCase(), resultSet.getString(i + 1));
                }
                resultSetVector.add(message);
            }
        } catch (Exception ex) {
        	logger.warn( "Exception arises while getting buffered messages  "+  ex.getLocalizedMessage() , ex);
        	throw ex;
         } finally {
        	DBSubmitConn.releaseDbConnection(null, null, conn, prep);
        }
        return resultSetVector;
    }

	public  void updateRecords() {
		String strQuery = "UPDATE " + CoreConstants.SENDER_TABLE_NAME + " SET STATUS = ? WHERE STATUS = ?";
		Connection con = null;
		PreparedStatement prep = null;
		int noOfRows = 0;
		try {
			con = DBSubmitConn.getconnetion();
			prep = con.prepareStatement(strQuery);
			prep.setString(1, "N");
			prep.setString(2, "P");
			noOfRows = prep.executeUpdate();
		} catch (Exception ex) {
			logger.warn( "Exception arises while updating previous pending records " , ex);
		} finally {
			DBSubmitConn.releaseDbConnection(null, null, con, prep);
		}
		
		if (noOfRows > 0) {
			logger.info("No of rows updated ---------------------> " + noOfRows);
		}
	}
}
