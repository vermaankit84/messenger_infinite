package com.messenger.conn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

public final class DBSubmitConn {
	private static DataSource dataSource = null;
	private static final Logger logger = Logger.getLogger(DBSubmitConn.class.getName());

	private DBSubmitConn() {
	}

	public static Connection getconnetion() {
		Connection conn = null;
		try {
			if (dataSource != null) {
				conn = dataSource.getConnection();
			}
		} catch (Exception e) {
			logger.warn("Exception arises while setting datasource", e);
		}
		return conn;
	}

	/**
	 * @param dataSource
	 *            the dataSource to set
	 */
	public static void setDataSource(DataSource dataSource) {
		DBSubmitConn.dataSource = dataSource;
	}

	public static void closeDataSource() {
		if (dataSource != null) {
			DBSubmitConn.dataSource = null;
		}
	}

	/**
	 * releasing the db connection
	 * 
	 * @param st
	 * @param rs
	 * @param conn
	 */
	public static void releaseDbConnection(Statement st, ResultSet rs, Connection conn, PreparedStatement psmt) {
		try {
			if (st != null) {
				st.close();
			}
		} catch (Exception e) {
			logger.warn("Exception arises while closing statement", e);
		}

		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.warn("Exception arises while closing rs", e);
		}

		try {
			if (psmt != null) {
				psmt.close();
			}
		} catch (Exception e) {
			logger.warn("Exception arises while closing psmt", e);
		}

		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			logger.warn("Exception arises while closing conn", e);
		}
	}
}
