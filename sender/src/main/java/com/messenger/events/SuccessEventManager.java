package com.messenger.events;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import org.apache.log4j.Logger;
import com.messenger.valueobjects.SuccessEventVO;
import com.messenger.conn.DBSubmitConn;
import com.messenger.queries.Queries;


public class SuccessEventManager implements Runnable {
	
	private static Hashtable<String, SuccessEventVO> mRepository = new Hashtable<>();
	private final Logger logger = Logger.getLogger(SuccessEventManager.class.getName());
	
	public SuccessEventManager() {	
	}

	public void run() {
		processMessages();
	}

	private void processMessages() {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		int[] noOfRows = null;
		Enumeration<String> hashenum = mRepository.keys();
		try {
			conn = DBSubmitConn.getconnetion();
			preparedStatement = conn.prepareStatement(Queries.STR_SUCESS_EVT_MGR);
			while (hashenum.hasMoreElements()) {
				String vendorID = hashenum.nextElement();
				if (vendorID != null) {
					SuccessEventVO successEventVO = mRepository.get(vendorID);
					int pPrimaryCount = successEventVO.getPrimaryCount();
					Calendar cal = Calendar.getInstance();				
					preparedStatement.setString(1, vendorID);			
					preparedStatement.setString(2, String.valueOf(cal.get(Calendar.DATE)));
					preparedStatement.setString(3, String.valueOf(cal.get(Calendar.MONTH) + 1));
					preparedStatement.setString(4, String.valueOf(cal.get(Calendar.YEAR)));
					preparedStatement.setString(5, String.valueOf(cal.get(Calendar.HOUR_OF_DAY)));
				    preparedStatement.setString(6, String.valueOf(cal.get(Calendar.MINUTE)));			    
					preparedStatement.setInt(7, pPrimaryCount);
					mRepository.remove(vendorID);
					preparedStatement.addBatch();
				}
			}
			noOfRows = preparedStatement.executeBatch();
			if (noOfRows != null && noOfRows.length > 0) {
				logger.info("------------ No of records inserted in success event manager --------> " + noOfRows.length);
			}
			
		} catch (Exception ex) {
			logger.warn("EXCEPTION ARISES IN SUCCESS EVENT MANAGER "+ ex.getLocalizedMessage() , ex);
		} finally {
			DBSubmitConn.releaseDbConnection(null, null, conn, preparedStatement);
		}
		
	}

	public static synchronized  void addRequest(String pVendorId, int pPrimaryCount, int pSecondaryCount) {
		if (mRepository.containsKey(pVendorId)) {
			SuccessEventVO objEntity = mRepository.get(pVendorId);
			if (pPrimaryCount > 0){
				objEntity.setPrimaryCount(objEntity.getPrimaryCount() + pPrimaryCount);
			}
			if (pSecondaryCount > 0) {
				objEntity.setSecondaryCount(objEntity.getSecondaryCount() + pSecondaryCount);
			}
			mRepository.put(pVendorId, objEntity);
		} else {
			mRepository.put(pVendorId, new SuccessEventVO(pVendorId, pPrimaryCount, pSecondaryCount));
		}
	}

	public static int getTotalCountInMin(String pVendorId) {
		int pRequestCount = 0;
		if (mRepository.contains(pVendorId)) {
			pRequestCount =  mRepository.get(pVendorId).getTotalCounts();
		}
		return pRequestCount;
	}

}
