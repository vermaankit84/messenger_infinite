package com.messenger.forwarder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Enumeration;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.messenger.conn.DBSubmitConn;
import com.messenger.properties.CoreProperties;
import com.messenger.queries.Queries;
import com.messenger.valueobjects.DeleteVO;

public class DeleteProcessMessages implements Runnable {

	private static Vector<DeleteVO> mRepository = new Vector<>();
	private static final Logger logger = Logger.getLogger(DeleteProcessMessages.class.getName());
	
	public DeleteProcessMessages() {
	
	}
	
	@Override
	public void run() {
		try {
			deleteMessages();
		} catch (Exception ex) {
			logger.warn("Exception arises " , ex);
		}
	}

	private static synchronized void deleteMessages() {
		Connection conn = null;
		PreparedStatement psmt = null;
		int batchSize = Integer.parseInt(CoreProperties.getInstance().getValue("DELETE_BATCH_SIZE"));
		String query = Queries.STR_DELETE_PROCESS_MESSAGES;

		int[] noOfRecordDeleted = null;
		try {
			if (mRepository != null && !mRepository.isEmpty()) {
				Enumeration<DeleteVO> hashenum = mRepository.elements();
				conn = DBSubmitConn.getconnetion();
				psmt = conn.prepareStatement(query);
				for (int i = 0; hashenum.hasMoreElements() && i < batchSize; i++) {
					DeleteVO key = hashenum.nextElement();
					psmt.setString(1, key.getpResponseId());
					psmt.setString(2, key.getpMsisdn());
					psmt.addBatch();
					mRepository.remove(key);
				}
				noOfRecordDeleted = psmt.executeBatch();
			}

		} catch (Exception e) {
			logger.warn("Exception arises while deleting messages from queues", e);
		} finally {
			DBSubmitConn.releaseDbConnection(null, null, conn, psmt);
		}
		if (noOfRecordDeleted!=null && noOfRecordDeleted.length > 0 ) {
			logger.info("No of records deleted are " + noOfRecordDeleted.length);
		}	
	}

	public static synchronized  void addDeleteMessage(String pResponseId,String pMsisdn) {
		mRepository.add(new DeleteVO(pResponseId, pMsisdn));
	}
}
