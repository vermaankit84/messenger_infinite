package com.messenger.forwarder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.messenger.conn.DBSubmitConn;
import com.messenger.queries.Queries;
import com.messenger.valueobjects.UnprocessedVO;


public class UpdateUnProcessedMessages implements Runnable {

	private static Hashtable<UnprocessedVO, String> mRepository = new Hashtable<>();;
	private final Logger logger = Logger.getLogger(UpdateUnProcessedMessages.class.getName());
	
	public UpdateUnProcessedMessages() {
		
	}
	@Override
	public void run() {
		reOpenMessages();
	}

	private void reOpenMessages() {
		Connection con = null;
		PreparedStatement psmt = null;
		int[] noOfRecordUpdated = null;
		try {
			if (mRepository != null && mRepository.size() > 0) {
				con = DBSubmitConn.getconnetion();
				if (con != null) {
					psmt = con.prepareStatement(Queries.STR_UPDATE_UNP_MESS);
					Enumeration<UnprocessedVO> hashenum = mRepository.keys();
					while (hashenum.hasMoreElements()) {
						UnprocessedVO key = hashenum.nextElement();
						psmt.setString(1, "N");
						psmt.setString(2, key.getpResponseId());
						psmt.setString(3, key.getpMsisdn());
						psmt.addBatch();
						mRepository.remove(key);
					}
					noOfRecordUpdated = psmt.executeBatch();
				}				
			}
		} catch (Exception e) {
			logger.warn("*********** MESSENGER EXCEPTION ARISES IN UPDATING RECORDS ************"+e.getLocalizedMessage() , e);
		} finally {
			DBSubmitConn.releaseDbConnection(null, null, con, psmt);
		}
		if (noOfRecordUpdated != null && noOfRecordUpdated.length > 0) {
			logger.info(" No of Unprocessed Record Ends at =   " + noOfRecordUpdated.length);
		}
	}

	public static synchronized  void addReOpenMessage(String pResponseId,String pMobile) {
		mRepository.put(new UnprocessedVO(pResponseId, pMobile), "1");
	}
}
