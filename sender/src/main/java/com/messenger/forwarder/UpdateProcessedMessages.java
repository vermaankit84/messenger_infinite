package com.messenger.forwarder;

import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import com.messenger.conn.DBSubmitConn;
import com.messenger.properties.CoreProperties;
import com.messenger.queries.Queries;

public class UpdateProcessedMessages extends Thread {

	private static List<UpdateRecord> mRepository = null;
	private  ExecutorService newFixedThreadPool = null;
	private boolean isRun=true;
	private final Logger logger = Logger.getLogger(UpdateProcessedMessages.class.getName());
	public UpdateProcessedMessages() {
		int threadpoolsize= Integer.parseInt(CoreProperties.getInstance().getValue("UPDATE_PROCESS_POOL_SIZE"));
		mRepository = new ArrayList<>();
		super.setName("MIS-THREAD");
		newFixedThreadPool= Executors.newFixedThreadPool(threadpoolsize ,new ThreadFactory() {
			final AtomicInteger threadNumber=new AtomicInteger(1);
				@Override
				public Thread newThread(Runnable arg0) {
					Thread t = new Thread(Thread.currentThread().getThreadGroup(),arg0,"pool-MISUpdateThread-"+threadNumber.getAndIncrement());
					if(t.isDaemon())
						t.setDaemon(false);
					if(t.getPriority()!=Thread.NORM_PRIORITY)
						t.setPriority(Thread.NORM_PRIORITY);
					return t;
				}
			});
	}

	@Override
	public void run() {
		int batchSize = Integer.parseInt(CoreProperties.getInstance().getValue("PROCESS_BATCH_SIZE"));
		int sleepTime = Integer.parseInt(CoreProperties.getInstance().getValue("UPDATE_PROCESS_SLEEP_TIME"));
		while (isRun) {
			try {
				misUpdate(batchSize);
				Thread.sleep(sleepTime);
			} catch (Exception e) {
				logger.warn("Exception arises while updating mis thread data " , e);
			}
		}
	}

	private void misUpdate(int batchSize) {
		try {
			final List<UpdateRecord> arrayList = new ArrayList<>();
			arrayList.addAll(mRepository);
			mRepository.removeAll(arrayList);
			if (!arrayList.isEmpty()) {
				int noBatches = arrayList.size() / batchSize;
				for (int i = 0; i < noBatches; i++) {
					ArrayList<UpdateRecord> updateBatchRecords = new ArrayList<UpdateRecord>();
					updateBatchRecords.addAll(arrayList.subList(0, batchSize));
					submitToPool(updateBatchRecords);
					arrayList.removeAll(updateBatchRecords);

				}
				if (!arrayList.isEmpty()) {
					submitToPool(arrayList);
				}
			}
		} catch (Exception e) {
			logger.warn("Exception arises while updating mis thread data " , e);
		}

	}

	public void submitToPool(final List<UpdateRecord> arrayList) {
		
		try {
			newFixedThreadPool.execute(new Runnable() {
				@Override
				public void run() {
					executeBatchUpdate(arrayList);
				}
			});
		} catch (RejectedExecutionException ex) {
			logger.warn ("---------- Number Of records found while updating the records ---------------------" + arrayList , ex);
			logger.fatal(arrayList.toString());
		}
	}

	public void executeBatchUpdate(final List<UpdateRecord> arrayList) {
		Connection con = null;
		PreparedStatement psmt = null;
		int[] noOfRecordPrcoceed = null;		
		try {			
			if (arrayList != null && !arrayList.isEmpty()) {			
				con = DBSubmitConn.getconnetion();
				psmt = con.prepareStatement(Queries.STR_INSERT_INTO_MIS);				
				for(UpdateRecord str: arrayList){
					if (str != null) {
						psmt.setString(1, str.responseId);
						psmt.setString(2, str.mobileNumber);
						psmt.setString(3,str.venderId);
						psmt.setString(4, str.status);
						psmt.setString(5, str.statusDesc);
						psmt.setString(6, str.vendorResponse);
						psmt.setString(7, str.dumpDate);
						psmt.setString(8, "".equalsIgnoreCase(str.time) ? null : str.time);
						psmt.addBatch();
					}
				}
				noOfRecordPrcoceed = psmt.executeBatch();
				logger.info("records updated in processed messages " + noOfRecordPrcoceed.length);		
			}
		}catch(BatchUpdateException be){
			logger.fatal(arrayList.toString());
			logger.warn("Exception arises in batch exception" , be);
		}catch(SQLException se){
			logger.fatal(arrayList.toString());
			logger.warn("Exception arises in batch exception" , se);
		}catch (Exception e) {
			logger.fatal(arrayList.toString());
			logger.warn("Exception arises in batch exception" , e);
		} finally {
			DBSubmitConn.releaseDbConnection(null, null, con, psmt);
		}
		
	}

	
	public static synchronized   void addCloseMessage(String vendorID , String status , String statusDesc , String vendorResponse , String mMobileNumber , String mResponseId , String responseTime, String dumpDate) {
		mRepository.add(new UpdateProcessedMessages.UpdateRecord(vendorID , status , statusDesc , vendorResponse , mResponseId , mMobileNumber,responseTime,dumpDate));
	}
	
	private static	class UpdateRecord {
		private String venderId = null;
		private String dumpDate = null;
		private String status = null;
		private String statusDesc = null;
		private String vendorResponse = null;
		private String responseId = null;
		private String mobileNumber = null;
		private String time = null;
		
		public  UpdateRecord(String venderId, String status, String statusDesc,String vendorResponse, String mResponseId, String mMobileNumber, String time, String dumpDate) {
			this.venderId = venderId;
			this.status = status;
			this.statusDesc = statusDesc;
			this.vendorResponse = vendorResponse;
			this.responseId = mResponseId;
			this.mobileNumber = mMobileNumber;
			this.time = time;
			this.dumpDate=dumpDate;
		}

		@Override
		public String toString() {
			return "UpdateRecord [venderId=" + venderId + ", status=" + status + ", statusDesc=" + statusDesc + ", vendorResponse=" + vendorResponse + ", responseId=" + responseId + ", mobileNumber=" + mobileNumber + ", time=" + time + ", dumpDate=" + dumpDate + "]";
		}
		
	}


	public void finish() throws InterruptedException {
		isRun = false;
		newFixedThreadPool.shutdown();
		newFixedThreadPool.awaitTermination(1, TimeUnit.SECONDS);
		if (!newFixedThreadPool.isShutdown()) {
			newFixedThreadPool.shutdownNow();
		}
	}
}
