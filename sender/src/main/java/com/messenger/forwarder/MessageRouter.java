package com.messenger.forwarder;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.messenger.dao.SenderDao;
import com.messenger.properties.CoreProperties;
import com.messenger.server.SenderServer;
import com.messenger.utils.Utilities;


public class MessageRouter implements Runnable {

    private final int poolSize = Integer.parseInt(CoreProperties.getInstance().getValue("THREADCHECK" , String.valueOf(60)));
    private boolean isRun = true;
    private ExecutorService pool = Executors.newFixedThreadPool(poolSize);
    private final Logger logger = Logger.getLogger(MessageRouter.class.getName());
    private final SenderDao senderDao = new SenderDao();
    
    public MessageRouter() {
    	try {
			senderDao.updateRecords();
		} catch (Exception e) {
			logger.warn("Exceptin arises while updating records at start application [ " + Utilities.getCurrentDateTime() + " ] ", e);
		}
    }
    @Override
    public void run() {
        while (isRun) {      	
            try {         	
                final List<Map<String, String>> messages = senderDao.getBufferedMessages();
                if (messages != null && !messages.isEmpty()) {
                    senderDao.doUpdateResponseId(messages);
                    submitData(messages);
                } else {
                	TimeUnit.SECONDS.sleep(1);
                }
            } catch (Exception ex) {
            	logger.warn("messenger No if records failed due to rejection are " , ex);
            	try {
					finish();
				} catch (InterruptedException e) {
				}
            }
        }
    }
	private void submitData(final List<Map<String, String>> messages) {
		for (int loop = 0; loop < messages.size(); loop++) {
		    try {
		        pool.submit(SenderServer.getInstance().createTask(messages.get(loop)));
		    } catch (Exception ex) {
		    	logger.warn("messenger No if records failed due to rejection are ----------->" + messages.size() , ex);
		    	senderDao.updateRejectedMessages(messages);
		    } 
		}
	}

	public void finish() throws InterruptedException {
		isRun = false;
		if (pool != null) {
			pool.awaitTermination(1, TimeUnit.SECONDS);
			pool.shutdown();
			if (!pool.isShutdown()) {
				pool.shutdownNow();
			}
		}
	}
}