package com.messenger.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;


public final class CoreProperties {

	private static CoreProperties mInstance = null;
	private final Properties mProperties = new Properties();
	private static final  Logger logger = Logger.getLogger(CoreProperties.class.getName());
	
	private CoreProperties() {
		loadConfiguration();
	}
	
	public static synchronized CoreProperties getInstance() {
		if (mInstance == null) {
			mInstance = new CoreProperties();
		}
		return mInstance;
	}
	
	private void loadConfiguration() {
		InputStream resourceAsStream = null;
		try {
			resourceAsStream  = Thread.currentThread().getContextClassLoader().getResourceAsStream("sub-coreconfig.properties");
			mProperties.load(resourceAsStream);
			logger.info("READ SENDER CORE APPLICATION CORE PROPERTY SUCCESSFULLY");
		} catch (IOException ex) {
			logger.warn("UNABLE TO SENDER CORE PROPERTY SUCCESSFULLY ::::" , ex);
		} finally {
			try {
				if (resourceAsStream != null) {
					resourceAsStream.close();
				}
			} catch (IOException e) {
				logger.warn("Exception arises while closinging stream", e);
			}
		}
	}

	public String getValue(String pKey) {
		return mProperties.getProperty(pKey);
	}
	
	public String getValue(String pKey , String defaultValue) {
		return mProperties.getProperty(pKey,defaultValue);
	}
}
