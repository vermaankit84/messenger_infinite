package com.messenger.task;

import java.util.Map;

import com.messenger.router.TaskFactory;
import com.messenger.send.ProcessRequest;
import com.messenger.sender.MessageSender;

public class SenderTaskFactory implements TaskFactory {

	@Override
	public Runnable createTask(final Map<String, String> message , final ProcessRequest processRequest) {
		return new MessageSender(message , processRequest);
	}
}
