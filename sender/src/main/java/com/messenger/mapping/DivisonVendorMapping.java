package com.messenger.mapping;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.messenger.conn.DBSubmitConn;
import com.messenger.repositories.VendorRepository;
import com.messenger.utils.Utilities;


public class DivisonVendorMapping {
	private  Map<String, List<Integer>> mRepository = new HashMap<>();
	private static DivisonVendorMapping mInstance = null;
	private static final Logger log = Logger.getLogger(DivisonVendorMapping.class.getName());
	public DivisonVendorMapping() {
		loadRepository();
	}

	public static synchronized  DivisonVendorMapping getInstance() {
		if (mInstance == null) {
			mInstance = new DivisonVendorMapping();
			
		}
		return mInstance;
	}

	private void loadRepository() {
		String strQuery = "select DIVISIONID , VENDORID from mess_division_vendor_mapper";
		Connection conn = null;
		Statement stat = null;
		ResultSet rs = null;
		List<String> result = null;
		
		try {
			conn = DBSubmitConn.getconnetion();
			stat = conn.createStatement();
			rs = stat.executeQuery(strQuery);
			result = Utilities.buildVectorFromResultSet(rs);
		} catch (Exception ex) {
			log.warn(" ****************** Exception arises in DivisonVendorMapping ****************** " , ex);
		} finally {
			DBSubmitConn.releaseDbConnection(stat, rs, conn, null);
		}
		
		if (result != null && !result.isEmpty()) {		
			mRepository.clear();
			for (int i = 0; i < result.size(); i = i + 2) {
				String divisonID = result.get(i + 0);
				int vendorId = Integer.parseInt(result.get(i + 1));
				List<Integer> vendorList = null;
				if(mRepository.containsKey(divisonID) && VendorRepository.getInstance().isvendorRunning(vendorId)){
					vendorList = mRepository.get(divisonID);
					vendorList.add(vendorId);
					mRepository.put(divisonID, vendorList);					
				} else {					
					vendorList = new ArrayList<>();
					vendorList.add(vendorId);
					mRepository.put(divisonID, vendorList);
				}
			}
		}
	}
	
	public List<Integer> getVendorList(final String divisionId, String messageType) {
		final List<Integer> vendorList = mRepository.get(divisionId);
		if (vendorList == null || vendorList.isEmpty()) {
			return null;
		}
		if ("S".equals(messageType)) {
			return getFilteredList(v -> VendorRepository.getInstance().isSmsSupportedVendor(v), vendorList);
		} else {
			return getFilteredList(v -> VendorRepository.getInstance().isEmailSupportedVendor(v), vendorList);
		}
	}
	
	public void reloadRepository() {
		loadRepository();
	}
	
	private  <T> List<T> getFilteredList(final Predicate<T> predicate , final List<T> data) {
		return data.stream().filter(predicate).collect(Collectors.toList());
	}
}
