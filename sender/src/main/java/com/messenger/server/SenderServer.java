package com.messenger.server;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.messenger.constants.CoreConstants;
import com.messenger.events.SuccessEventManager;
import com.messenger.forwarder.DeleteProcessMessages;
import com.messenger.forwarder.MessageRouter;
import com.messenger.forwarder.UpdateProcessedMessages;
import com.messenger.forwarder.UpdateUnProcessedMessages;
import com.messenger.pool.factory.ScheduledPoolFactory;
import com.messenger.send.ProcessRequest;
import com.messenger.task.SenderTaskFactory;
import com.messenger.utils.Utilities;

public final class SenderServer {

    private static SenderServer instance = null;
    private final UpdateProcessedMessages updateProcessedMessages = new UpdateProcessedMessages();
    private final MessageRouter messageRouter = new MessageRouter();
    private SenderTaskFactory submitterTaskFactory = null ;
    private final Logger looger = Logger.getLogger(SenderServer.class.getName());
    private final ProcessRequest processRequest = new ProcessRequest();
    
    public static synchronized SenderServer getInstance() {
        if (instance == null) {
            instance = new SenderServer();
        }
        return instance;
    }

    private  void intialtizeRunnableTaskFactory()  {
        this.submitterTaskFactory = new SenderTaskFactory();       
    }

    public Runnable createTask(Map<String, String> message) {
        return submitterTaskFactory.createTask(message , processRequest);

    }

    private SenderServer()  {
        intialtizeRunnableTaskFactory();
    }

    public String startSubmitter() {
        ScheduledPoolFactory.getScheduledThreadPool().getScheduledPool("updateUnProcessedMessages").scheduleWithFixedDelay(new UpdateUnProcessedMessages(), 0, 250, TimeUnit.MILLISECONDS);
        ScheduledPoolFactory.getScheduledThreadPool().getScheduledPool("deleteProcessMessages").scheduleWithFixedDelay(new DeleteProcessMessages(), 0, 250, TimeUnit.MILLISECONDS);
        ScheduledPoolFactory.getScheduledThreadPool().getScheduledPool("successEventManager").scheduleWithFixedDelay(new SuccessEventManager(), 0, 1, TimeUnit.MINUTES);
        ScheduledPoolFactory.getScheduledThreadPool().getScheduledPool("messageRouter").submit(messageRouter);
        updateProcessedMessages.start();
        return ("Queue has been started successfully for " + CoreConstants.SENDER_TABLE_NAME + " at " + Utilities.getCurrentDateTime());
    }

    public void stopSubmitter() throws InterruptedException {
        messageRouter.finish();
        updateProcessedMessages.finish();
        ScheduledPoolFactory.getScheduledThreadPool().finish();
        looger.info("System is getting shut down now [ " + Utilities.getCurrentDateTime() + " ] ");
    }
}
