package com.messenger.response.vendor.ez;

/**
 * Created by ankverma on 11/18/2016.
 */
public class PhoneNumbers {
    private String PhoneNumber;

    public String getPhoneNumber ()
    {
        return PhoneNumber;
    }

    public void setPhoneNumber (String PhoneNumber)
    {
        this.PhoneNumber = PhoneNumber;
    }

    @Override
    public String toString()
    {
        return "PhoneNumbers [PhoneNumber = "+PhoneNumber+"]";
    }
}
