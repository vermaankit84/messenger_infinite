package com.messenger.response.vendor.zang;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import com.messenger.response.vendor.zang.SmsMessage;

@XmlRootElement(name = "Response")
public class Response
{
    @XmlElement
    private SmsMessage SmsMessage;

    public SmsMessage getSmsMessage ()
    {
        return SmsMessage;
    }

    public void setSmsMessage (SmsMessage SmsMessage)
    {
        this.SmsMessage = SmsMessage;
    }

    @Override
    public String toString()
    {
        return "Response [SmsMessage = "+SmsMessage+"]";
    }
}
