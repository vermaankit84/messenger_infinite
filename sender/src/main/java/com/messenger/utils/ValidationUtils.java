package com.messenger.utils;

import com.messenger.repositories.SenderRepository;
import com.messenger.repositories.TemplateRepository;

public final class ValidationUtils {
	private ValidationUtils() {}
	
	public static String validateRequest(String mMessageType, String mDivisionId,String mMessage, String mMobileNumber,String mResponseId, String dumpDate, String mSender) {
		if ("S".equalsIgnoreCase(mMessageType) && !Utilities.validateMSISDN(mMobileNumber)) {
			return "Invalid Msisdn Obtained";
		}
	   		
		if (!"E".equalsIgnoreCase(mMessageType) && !TemplateRepository.getInstance().istemplateMatched(mDivisionId, mMessage)) {
			return "Invalid Template Obtained";
		}

		if (!SenderRepository.getInstance().isValidSender(mDivisionId, mSender)) {
			return "Invalid Sender Obtained";
		}
		if ("E".equalsIgnoreCase(mMessageType) && !Utilities.validateEmail(mMobileNumber)) {
			return "Invalid Email Id Obtained";
		}
		return null;
	}
}
