package com.messenger.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Vector;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class Utilities {
	
	private Utilities() {
		 
	}
		  
	public final static String getCurrentDateTime() {
		GregorianCalendar cal = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String curDate = sdf.format(cal.getTime());
		return curDate;
	}

	public final static Vector<String> buildVectorFromResultSet(ResultSet resultSet) throws SQLException {
		Vector<String> resultSetVector = null;
		ResultSetMetaData rsmDataObject = resultSet.getMetaData();
		while (resultSet.next()) {
			if (resultSetVector == null) {
				resultSetVector = new Vector<String>();
			}
			for (int i = 0; i < rsmDataObject.getColumnCount(); i++)
				resultSetVector.addElement(resultSet.getString(i + 1));
		}

		return resultSetVector;
	}
	
	
	

	public final static boolean validateMSISDN(String mMobileNumber) {
		boolean isValidmsisdn = true;
		try {
			Long.parseLong(mMobileNumber);
		} catch (NumberFormatException ex) {
			isValidmsisdn = false;
		} catch (Exception ex) {
			isValidmsisdn = false;
		}
		return isValidmsisdn;
	}
	
	public final static boolean validateEmail(final String emailAdd) {		
		return Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$").matcher(emailAdd).matches();
	}
	
	public static Map<String, String> getNameValuePair(String[] str) {
		return Arrays.stream(str).collect(Collectors.toMap(new Function<String, String>() {
			@Override
			public String apply(String s) {
				return s.split("\\=")[0];
			}
		}, new Function<String, String>() {
			@Override
			public String apply(String s) {
				return s.split("\\=")[1];
			}
		}));
	}
	
	public static String encode(final String msgtext) throws UnsupportedEncodingException {
		if (msgtext == null) {
			return null;
		}
		return URLEncoder.encode(msgtext , "ISO-8859-1");
	}
	
	public static String decode(final String msgtext) throws UnsupportedEncodingException {
		if (msgtext == null) {
			return null;
		}
		return URLDecoder.decode(msgtext , "ISO-8859-1");
	}
}
