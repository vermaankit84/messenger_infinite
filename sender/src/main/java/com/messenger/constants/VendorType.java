package com.messenger.constants;

public enum VendorType {
	SMS_OTP(1)  , SMS_TRANS(2) , SMS_PROMO(3) , EMAIL_OTP(4) , EMAIL_TRANS(5) , EMAIL_PROMO(6) , SMS_EMAIL_OTP(7), SMS_EMAIL_TRANS(8), SMS_EMAIL_PROMO(9);

	private int vendorCode = 0;

	private VendorType(final int vendorCode) {
		this.vendorCode = vendorCode;
	}

	public int getVendorCode() {
		return this.vendorCode;
	}
	
	public static VendorType getVendorType(final int vendorCode) {
		VendorType v = null;
		final VendorType[] vendorTypes = VendorType.values();
		for (VendorType vendorType : vendorTypes) {
			if (vendorCode == vendorType.vendorCode) {
				v = vendorType;
				break;
			}
		}		
		return v;
	}
}
