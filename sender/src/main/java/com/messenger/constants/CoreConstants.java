package com.messenger.constants;

public interface CoreConstants {
	String PROCESS_SUBMIT = "S";// SUBMIT TO SMSC
	String MESSAGE_STATUS_PROGRESS = "P";
	String PROCESS_SUBMIT_DESC = "SUBMITTED TO SMSC";
	String SENDER_TABLE_NAME = System.getenv("mess_table_name");
}