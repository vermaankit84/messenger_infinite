package com.messenger.constants;

public enum MessageCategories {
	OTP_MESSAGE(1) , TRANS_MESSAGE(2) , PROMO_MESSAGE(3);
	
	private int messcat = 0;
	
	private MessageCategories (final int messcat) {
		this.messcat = messcat;
	}
	
	public static MessageCategories getMessageCategory(final int messType) {
		final MessageCategories[] messageCategories = MessageCategories.values();
		for (final MessageCategories m : messageCategories) {
			if (m.messcat == messType) {
				return m;
			}
		}
		return MessageCategories.PROMO_MESSAGE;
	}
	public static int getMessageType (final MessageCategories messageCategory) {
		final MessageCategories[] messageCategories = MessageCategories.values();
		for (final MessageCategories m : messageCategories) {
			if (messageCategory.equals(m)) {
				return m.messcat;
			}
		}
		return 3;
	}
}
