package com.messenger.pool.factory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class ScheduledPoolFactory {
	private Map<String, ScheduledExecutorService> scheduledMap = new HashMap<>();
	private static ScheduledPoolFactory pool = null;
	
	public synchronized static ScheduledPoolFactory getScheduledThreadPool () {
		if (pool == null) {
			pool = new ScheduledPoolFactory();
		}		
		return pool;
	}
	
	private ScheduledPoolFactory() {
		 buildThreadPool();
	}
	
	private void buildThreadPool() {
		scheduledMap.put("successEventManager", Executors.newScheduledThreadPool(1));
		scheduledMap.put("updateProcessedMessages", Executors.newScheduledThreadPool(1));
		scheduledMap.put("updateUnProcessedMessages", Executors.newScheduledThreadPool(1));
		scheduledMap.put("deleteProcessMessages", Executors.newScheduledThreadPool(1));
		scheduledMap.put("messageRouter", Executors.newScheduledThreadPool(1));
	}
	
	
	public ScheduledExecutorService getScheduledPool (String identifier) {
		return scheduledMap.get(identifier);
	}
	
	
	public void finish () {
		scheduledMap.keySet().forEach(new Consumer<String>() {
			@Override
			public void accept(final String key) {
				final ScheduledExecutorService scheduledExecutorService = scheduledMap.get(key);
				shutDownService(scheduledExecutorService);
			}

			private void shutDownService(ScheduledExecutorService scheduledExecutorService) {
				try {
					scheduledExecutorService.shutdown();
					scheduledExecutorService.awaitTermination(1, TimeUnit.SECONDS);				
					if (!scheduledExecutorService.isShutdown()) {
						scheduledExecutorService.shutdownNow();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}			
			}
		});
	}

}
